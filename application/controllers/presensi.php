<?php
class Presensi extends CI_controller
{
    public $model = null;
    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION['role']))
		{
			return redirect(base_url() . 'login/admin');
		}
        //memuat model
        $this->load->model('presensi_model');
        $this->model = $this->presensi_model;
        //memuat library database
        $this->load->database();
    }

    public function tapping()
    {
      // handle request
      if(isset($_POST['btnsubmit']))
      {
        $_POST['id_hari'] = date('w');
        $_POST['tanggal'] = date('Y-m-d'); 
                
        if($this->model->insert())
        {
            $this->session->set_flashdata('status', 'success');
        }
        else
        {
            $this->session->set_flashdata('status', 'failed');
        }            
      }

      // handle view
      $data = $this->layout();
      $data['sub_breadcrumbs_title'] = "Tapping";
      $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
      $this->load->view('pa/presensi/presensi_tapping', $data);
    }

    public function setting()
    {
        // handle request

        // handle view  
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Presensi Setting";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
        $data['rows'] = $this->db->get('hari')->result(); 
        $this->load->view('pa/presensi/presensi_setting', $data);
    }

    public function edit($id)
    {
      // handle request
      if(isset($_POST['btnsubmit']))
      {
          $sql = sprintf(
            "UPDATE hari SET 
            jam_tiba_mulai ='%s',
            jam_tiba_selesai ='%s',
            jam_pulang_mulai ='%s',
            jam_pulang_selesai = '%s' 
            where id='%d'",
            $this->input->post('jam_tiba_mulai'),
            $this->input->post('jam_tiba_selesai'),
            $this->input->post('jam_pulang_mulai'),
            $this->input->post('jam_pulang_selesai'),
            $this->input->post('id')
            );
            $this->db->query($sql);

            return redirect('presensi/setting');
      }

      // handle view  
      $data = $this->layout();
      $data['sub_breadcrumbs_title'] = "Presensi Setting Edit";
      $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
      $data['rows'] = $this->db->get_where('hari', array('id' => $id))->result();
      $this->load->view('pa/presensi/presensi_setting_edit', $data);
    }

    public function riwayat($id_pegawai = null, $month = null, $year = null)
    {
        if($month == null)
            $month = (new DateTime('now'))->format('m');

        if($year == null)
            $year = (new DateTime('now'))->format('Y');

        
        // handle view  
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Presensi Riwayat";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
        $this->db->join('pegawai', 'presensi.id_pegawai = pegawai.id_pegawai');
        
        if($id_pegawai && $month && $year)
        {
            $this->db->where('pegawai.id_pegawai', $id_pegawai);
            $this->db->where('MONTH(tanggal)', $month);
            $this->db->where('YEAR(tanggal)', $year);
        }  

        $data['id_pegawai'] = $id_pegawai;
        $data['year'] = $year;
        $data['month'] = $month;
        $data['rows'] = $this->db->get('presensi')->result(); 
        $data['haris'] = $this->db->get('hari')->result();
        $data['pegawai'] = $this->db->query("SELECT * FROM pegawai WHERE id_pegawai = '$id_pegawai'")->row();

        if($id_pegawai)
            $this->load->view('pa/presensi/presensi_read_specific_view', $data);
        else
            $this->load->view('pa/presensi/presensi_read_view', $data);
    }    

    public function manage()
    {
        // handle request

        // handle view  
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Presensi Manage";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
        $this->db->join('pegawai', 'presensi.id_pegawai = pegawai.id_pegawai');
        $data['rows'] = $this->db->get('presensi')->result(); 
        $this->load->view('pa/presensi/presensi_manage', $data);
    }

    public function create()
    {
        // handle request
        if(isset($_POST['btnsubmit']))
        {
            $exist = $this->db->query("SELECT * FROM presensi WHERE tanggal = '$_POST[tanggal]'")->result();
            $id_hari = date('w', strtotime($_POST['tanggal']));                 
            
            if(count($exist) == 0)
            {
                $action = $this->db->query("INSERT INTO presensi (id_hari, tanggal, waktu_masuk, waktu_keluar, id_pegawai) VALUES ('$id_hari', '$_POST[tanggal]', '$_POST[jam_masuk]', '$_POST[jam_keluar]', '$_POST[id_pegawai]')");
            } else
            {
                $action = $this->db->query("UPDATE presensi SET id_hari=$id_hari, tanggal='$_POST[tanggal]', waktu_masuk='$_POST[jam_masuk]', waktu_keluar='$_POST[jam_keluar]', id_pegawai='$_POST[id_pegawai]') WHERE id_pegawai='$_POST[id_pegawai]' AND tanggal='$_POST[tanggal]");
            }

            return redirect('presensi/manage');
        }

        // handle view  
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Presensi Create";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
        $data['rows'] = $this->db->get('pegawai')->result(); 
        $this->load->view('pa/presensi/presensi_manage_create', $data);
    }

    public function index()
    {
      $data = $this->layout();
      $data['sub_breadcrumbs_title'] = "Lihat Riwayat Izin";
      $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
      $data['rows'] = $this->model->read();
      $this->load->view('pa/iizin/zin_read_view', $data);
    }

    public function layout()
    {
        // Header
        $data['title'] = "Kinicheese Tea - Presensi";
        $data['breadcrumbs_title'] = "Presensi";
        $data['head'] = $this->load->view('layout/head', $data, TRUE);
        $data['header'] = $this->load->view('layout/header', NULL, TRUE);
        $data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);


        // Footer
        $data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
        $data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

        return $data;
    }

    public function insert()
    {
        $this->load->model('bahan_model');
        $this->bahan_model->insert();
    }

    public function storecreate()
    {
        $data = [];
        $rules = [

            array(
                'field'
                => 'id_bahan_baku',
                'label' => 'ID Bahan Baku',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi !',
                    'alpha_numeric_spaces'  => '%s hanya angka 8-9 atau huruf A-Z'
                )
            ),
            array(
                'field' => 'nama_bahan_baku',
                'label' => 'Nama Bahan Baku',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi !',
                    'alpha'  => '%s hanya huruf A-Z'
                )
            ),
            array(
                'field' => 'satuan',
                'label' => 'Satuan',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi !',
                    'numeric'  => '%s hanya angka'
                )
            ),
            array(
                'field' => 'harga_satuan',
                'label' => 'Harga Satuan',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi bro !',
                    'numeric'  => '%s hanya angka'
                )
            ),

        ];

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $data = $this->layout();
            $data['sub_breadcrumbs_title'] = "Tambah Bahan Baku";
            $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

            $last_id = $this->model->get_last_row()[0]->id_bahan_baku;
            $id_number = (int) substr($last_id, 1, 3);
            $id_number++;
            $id_number = (string) $id_number;
            if (strlen($id_number) == 1)
                $id_string = 'B00' . $id_number;
            else if (strlen($id_number) == 2)
                $id_string = 'B0' . $id_number;
            else
                $id_string = 'B' .  $id_number;

            $data['model'] = $this->model;
            $data['id_string'] = $id_string;
            $this->load->view('bahan_create_view', $data);
        } else {
            $this->insert();
            redirect(site_url('bahan'));
        }
    }

    public function storeupdate()
    {
        $rules =
            [
                [
                    'field' => 'id_bahan_baku',
                    'label' => 'id_bahan_baku',
                    'rules' => 'required|alpha_numeric',
                    'errors' => [
                        'required' => "%s harus diisi",
                        'Alpha_numeric' => "%s Hanya boleh berisikan huruf & angka (tidak boleh spasi)",
                    ]
                ],


                [
                    'field' => 'nama_bahan_baku',
                    'label' => 'nama_bahan_baku',
                    'rules' => 'required|alpha',
                    'errors' => [
                        'required' => "%s harus diisi",
                        'alpha' => "%s hanya huruf a-z"
                    ]
                ],


                [
                    'field' => 'satuan',
                    'label' => 'satuan',
                    'rules' => 'required',
                    'errors' => [
                        'required' => "%s harus diisi",
                    ]
                ],
                [
                    'field' => 'harga_satuan',
                    'label' => 'harga_satuan',
                    'rules' => 'required|alpha_numeric',
                    'errors' => [
                        'required' => "%s harus diisi",
                        'Alpha_numeric' => "%s Hanya boleh berisikan huruf & angka (tidak boleh spasi)",
                    ]
                ]


            ];
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $data = $this->layout();
            $data['sub_breadcrumbs_title'] = "Ubah Bahan Baku";
            $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

            $this->load->view('bahan_create_view', $data);
        } else {

            $this->load->model('bahan_model');
            $this->minuman_model->update();
            redirect('bahan');
        }
    }

    public function update($id)
    {
        //belum implementasi
        if (isset($_POST['btnsubmit'])) {
            $this->model->id_bahan_baku = $_POST['id_bahan_baku'];
            $this->model->nama_bahan_baku = $_POST['nama_bahan_baku'];
            $this->model->satuan = $_POST['satuan'];
            $this->model->harga_satuan = $_POST['harga_satuan'];

            $this->model->update();
            redirect('bahan');
        } else {
            $query = $this->db->query("SELECT * FROM bahan_baku WHERE id_bahan_baku='$id'");
            if ($query->num_rows() > 0) {

                $row = $this->layout();

                $row['row'] = $query->row();

                $row['sub_breadcrumbs_title'] = "Ubah Bahan Baku";
                $row['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $row, TRUE);
                $this->load->view('bahan_update_view', $row);
            } else {
                echo "<script>alert('TIDAK KETEMU')</script>";
                $this->load->view('bahan_update_view', ['model' => $this->model]);
            }
        }
    }

    public function delete($id)
    {
        //menentukan kode yang akan di hapus
        $this->model->id = $id;
        //menghapus baris data didalam tabel barang
        $this->model->delete();
        //mengarahkan kembali kehalaman utama/index
        redirect('bahan');
    }
}

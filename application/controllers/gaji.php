<?php
function weekOfMo($date) {
    //Get the first day of the month.
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    //Apply above formula.
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
}

class gaji extends CI_controller
{
    public $model = null;
    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION['role']))
		{
			return redirect(base_url() . 'login/admin');
		}
        //memuat model
        $this->load->model('gaji_model');
        $this->model = $this->gaji_model;
        //memuat library database
        $this->load->database();
    }    

    public function list()
    {
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "List";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);        
        $data['rows'] = $this->db->get('pegawai')->result();
        $this->load->view('pa/gaji/gaji_pegawai_list', $data);
    }

    function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count;
    }

    public function konfirmasi($id, $month, $year)
    {        
      // handle request
      if(isset($_POST['btnsubmit']))
      {         
        $last = $this->db->query("SELECT id_transaksi FROM jurnal_umum ORDER BY id_transaksi DESC LIMIT 1")->result()[0]->id_transaksi;
        
        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 506,
            'posisi_d_c' => 'd',
            'nominal' => $_POST['bonus'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 214,
            'posisi_d_c' => 'c',
            'nominal' => $_POST['bonus'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);        

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 214,
            'posisi_d_c' => 'd',
            'nominal' => $_POST['bonus'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);  

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 111,
            'posisi_d_c' => 'c',
            'nominal' => $_POST['bonus'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);  

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 511,
            'posisi_d_c' => 'd',
            'nominal' => $_POST['gaji_pokok'] - $_POST['total_denda'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 213,
            'posisi_d_c' => 'c',
            'nominal' => $_POST['gaji_pokok'] - $_POST['total_denda'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 213,
            'posisi_d_c' => 'd',
            'nominal' => $_POST['gaji_pokok'] - $_POST['total_denda'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 111,
            'posisi_d_c' => 'c',
            'nominal' => $_POST['gaji_pokok'] - $_POST['total_denda'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 215,
            'posisi_d_c' => 'd',
            'nominal' => $_POST['total_tunjangan'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);  

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 215,
            'posisi_d_c' => 'c',
            'nominal' => $_POST['total_tunjangan'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);  
        
        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 114,
            'posisi_d_c' => 'd',
            'nominal' => $_POST['total_utang'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);  

        $jurnal_umum_d = [
            'id_transaksi' => ++$last,
            'kode_akun' => 114,
            'posisi_d_c' => 'c',
            'nominal' => $_POST['total_utang'],
            'transaksi' => 'penggajian'
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);  

        $data = [
			'total' => $_POST['total_gaji'],			
            'bulan' => $month,	
            'tahun' => $year,			
            'id_pegawai' => $_POST['id_pegawai']		
		];
        $this->db->insert('gaji', $data);        
        return redirect('/gaji/riwayat');
      }

      // telat & bolos
      $telats = 0;
      $boloss = 0;
      $this->db->where('id_pegawai', $id);
      $this->db->where('MONTH(tanggal)', $month);
      $this->db->where('YEAR(tanggal)', $year);
      $presensis = $this->db->get('presensi')->result();
      $minggu = [0, 0, 0, 0];

      foreach($presensis as $p)
      {
          $fulldate = strtotime($p->tanggal);
          $date = date('d', $fulldate);
          $datee = strtotime( str_replace("/", "-", $date));
          $week = weekOfMo($datee);
          $dayofweek = date('D', $fulldate);

          switch($dayofweek)
          {
              case 'Mon':
                  $dayofweek = 1;
              break;
              case 'Tue':
                  $dayofweek = 2;
              break;
              case 'Wed':
                  $dayofweek = 3;
              break;
              case 'Thu':
                  $dayofweek = 4;
              break;
              case 'Fri':
                  $dayofweek = 5;
              break;
              case 'Sat':
                  $dayofweek = 6;
              break;
              case 'Sun':
                  $dayofweek = 7;
              break;
          }

          // get rule
          $rule = $this->db->get_where('hari', array('id' => $dayofweek))->result();

          $jamtibamulai = $rule[0]->jam_tiba_mulai;
          $jamtibaselesai = $rule[0]->jam_tiba_selesai;
          $jampulangmulai = $rule[0]->jam_pulang_mulai;
          $jampulangselesai = $rule[0]->jam_pulang_selesai;

          $presensitiba = $p->waktu_masuk;
          $presensipulang = $p->waktu_keluar;

          if($presensitiba > $jamtibaselesai)
          {
              $diff = ((strtotime($presensitiba) - strtotime($jamtibaselesai)) / 60) / 15;
              
              if($diff <= 3)
              {
                  $telats += floor($diff);
                  $minggu[$week-1]++;
              }
          } else {
              $minggu[$week-1]++;
          }
      }

      $totalDenda = $telats * 5000;

      foreach($minggu as $k => $m)
      {
        $bolos = 6 - $m;
        $boloss += $bolos;

        $totalDenda += $bolos * 25000;
      }
      
      // handle view
      $data = $this->layout();
      $data['sub_breadcrumbs_title'] = "gaji";
      $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
      
      $this->db->where('id_pegawai', $id);
      $data['pegawai'] = $this->db->get('pegawai')->result()[0];

      // izin
      $izins = $this->db->query("SELECT * FROM izin WHERE status = 'diizinkan' AND id_pegawai='$id' AND ((MONTH(tanggal_mulai) = '$month' AND YEAR(tanggal_mulai) = '$year') OR (MONTH(tanggal_selesai) = '$month' AND YEAR(tanggal_selesai) = '$year'))")->result();

      $totalIzin = 0;

      foreach($izins as $izin)
      {
        $mulai = new DateTime($izin->tanggal_mulai);
        $akhir = new DateTime($izin->tanggal_selesai);

        $diff =  $mulai->diff($akhir)->d;
        $totalIzin += $diff + 1;

        // print_r($izin);
      }

      // gaji pokok
      $data['gaji_pokok'] = $this->db->get_where('jabatan', array('id' => $data['pegawai']->id_jabatan))->result()[0]->gaji_pokok - ($totalIzin * 25000);
      $data['tunjangans'] = $this->db->get_where('tunjangan', array('id_pegawai' => $id))->result();
      $data['total_denda'] = $totalDenda;
      $data['total_telat'] = $telats * 15;
      $data['total_bolos'] = $boloss;

      // bonus
      $bonusKali = $this->db->get('bonus')->result()[0]->nominal;
      $jumlahPegawai = $this->db->query('SELECT COUNT(*) as jumlah FROM pegawai')->result()[0]->jumlah;

      // check if exists
      $exist = $this->db->get_where('gaji', array('bulan' => $month, 'tahun' => $year, 'id_pegawai' => $id))->result();
      $data['exist'] = 0;

      if(sizeof($exist) > 0)
      {
          $data['exist'] = 1;
      }

      $data['month'] = $month;
      $data['year'] = $year;

      // total penjualan bulan ini
      $totalPenjualan = $this->db->query('SELECT COUNT(jumlah) as jumlah FROM nota_penjualan WHERE MONTH(tgl_jual) = "' . $month . '" AND YEAR(tgl_jual) = "' . $year . '"')->result()[0]->jumlah;
      $data['bonus'] = ($totalPenjualan/30 * $bonusKali) / $jumlahPegawai;
      $data['utangs'] = $this->db->query("SELECT * FROM piutang WHERE id_pegawai = '$id' AND MONTH(tanggal) = '$month' AND YEAR(tanggal) = '$year'")->result();      

      $this->load->view('pa/gaji/gaji_konfirmasi', $data);
    }

    public function slip($id_gaji)
    {
    $gaji = $this->db->query("SELECT MONTH(waktu_generate) as month, YEAR(waktu_generate) as year, waktu_generate, total, gaji.id_pegawai, pegawai.* FROM gaji JOIN pegawai ON pegawai.id_pegawai = gaji.id_pegawai WHERE id = '$id_gaji'")->row();
    $id_pegawai = $gaji->id_pegawai;
    $month = $gaji->month;
    $year = $gaji->year;

    $data['gaji'] = $gaji;

    // telat & bolos
    $telats = 0;
    $boloss = 0;
    $this->db->where('id_pegawai', $id_pegawai);
    $this->db->where('MONTH(tanggal)', $month);
    $this->db->where('YEAR(tanggal)', $year);
    $presensis = $this->db->get('presensi')->result();
    $minggu = [0, 0, 0, 0];

    foreach($presensis as $p)
    {
        $fulldate = strtotime($p->tanggal);
        $date = date('d', $fulldate);
        $datee = strtotime( str_replace("/", "-", $date));
        $week = weekOfMo($datee);
        $dayofweek = date('D', $fulldate);

        switch($dayofweek)
        {
            case 'Mon':
                $dayofweek = 1;
            break;
            case 'Tue':
                $dayofweek = 2;
            break;
            case 'Wed':
                $dayofweek = 3;
            break;
            case 'Thu':
                $dayofweek = 4;
            break;
            case 'Fri':
                $dayofweek = 5;
            break;
            case 'Sat':
                $dayofweek = 6;
            break;
            case 'Sun':
                $dayofweek = 7;
            break;
        }

        // get rule
        $rule = $this->db->get_where('hari', array('id' => $dayofweek))->result();

        $jamtibamulai = $rule[0]->jam_tiba_mulai;
        $jamtibaselesai = $rule[0]->jam_tiba_selesai;
        $jampulangmulai = $rule[0]->jam_pulang_mulai;
        $jampulangselesai = $rule[0]->jam_pulang_selesai;

        $presensitiba = $p->waktu_masuk;
        $presensipulang = $p->waktu_keluar;

        if($presensitiba > $jamtibaselesai)
        {
            $diff = ((strtotime($presensitiba) - strtotime($jamtibaselesai)) / 60) / 15;
            
            if($diff <= 3)
            {
                $telats += floor($diff);
                $minggu[$week-1]++;
            }
        } else {
            $minggu[$week-1]++;
        }
    }

    $totalDenda = $telats * 5000;

    foreach($minggu as $k => $m)
    {
      $bolos = 6 - $m;
      $boloss += $bolos;

      $totalDenda += $bolos * 25000;
    }

    // izin
    $izins = $this->db->query("SELECT * FROM izin WHERE status = 'diizinkan' AND id_pegawai='$id_pegawai' AND ((MONTH(tanggal_mulai) = '$month' AND YEAR(tanggal_mulai) = '$year') OR (MONTH(tanggal_selesai) = '$month' AND YEAR(tanggal_selesai) = '$year'))")->result();

    $totalIzin = 0;

    foreach($izins as $izin)
    {
      $mulai = new DateTime($izin->tanggal_mulai);
      $akhir = new DateTime($izin->tanggal_selesai);

      $diff =  $mulai->diff($akhir)->d;
      $totalIzin += $diff + 1;

      // print_r($izin);
    }

      // gaji pokok
      $data['gaji_pokok'] = $this->db->get_where('jabatan', array('id' => $gaji->id_jabatan))->result()[0]->gaji_pokok - ($totalIzin * 25000);
      $data['tunjangans'] = $this->db->get_where('tunjangan', array('id_pegawai' => $id_pegawai))->result();
      $data['total_denda'] = $totalDenda;
      $data['total_telat'] = $telats * 15;
      $data['total_bolos'] = $boloss;

      // bonus
      $bonusKali = $this->db->get('bonus')->result()[0]->nominal;
      $jumlahPegawai = $this->db->query('SELECT COUNT(*) as jumlah FROM pegawai')->result()[0]->jumlah;
      $totalPenjualan = $this->db->query('SELECT COUNT(jumlah) as jumlah FROM nota_penjualan WHERE MONTH(tgl_jual) = "' . $month . '" AND YEAR(tgl_jual) = "' . $year . '"')->result()[0]->jumlah;
      $data['bonus'] = ($totalPenjualan/30 * $bonusKali) / $jumlahPegawai;

      // utang
      $data['utangs'] = $this->db->query("SELECT * FROM piutang WHERE id_pegawai = '$id_pegawai' AND MONTH(tanggal) = '$gaji->month' AND YEAR(tanggal) = '$gaji->year'")->result();      

      $this->load->view('pa/gaji/cetakslip', $data);
    }

    public function riwayat()
    {
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Lihat gaji";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);        
        $this->db->join('pegawai', 'gaji.id_pegawai = pegawai.id_pegawai');
        $data['rows'] = $this->db->get('gaji')->result();
        $this->load->view('pa/gaji/gaji_riwayat', $data);
    }

    public function edit($id)
    {
      // handle request
      if(isset($_POST['btnsubmit']))
      {
          $sql = sprintf(
            "UPDATE gaji SET 
            nama ='%s',
            nominal ='%s',
            jumlah ='%s',
            tipe = '%s' 
            where id='%d'",
            $this->input->post('nama'),
            $this->input->post('nominal'),
            $this->input->post('jumlah'),
            $this->input->post('tipe'),
            $this->input->post('id')
            );
            $this->db->query($sql);

            return redirect('gaji/riwayat');
      }

      // handle view  
      $data = $this->layout();
      $data['sub_breadcrumbs_title'] = "gaji riwayat Edit";
      $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
      $data['rows'] = $this->db->get_where('gaji', array('id' => $id))->result();
      $this->load->view('pa/gaji/gaji_riwayat_edit', $data);
    }

       
    public function layout()
    {
        // Header
        $data['title'] = "Kinicheese Tea - gaji";
        $data['breadcrumbs_title'] = "gaji";
        $data['head'] = $this->load->view('layout/head', $data, TRUE);
        $data['header'] = $this->load->view('layout/header', NULL, TRUE);
        $data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);


        // Footer
        $data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
        $data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

        return $data;
    }

    public function insert()
    {
        $this->load->model('bahan_model');
        $this->bahan_model->insert();
    }

    public function storecreate()
    {
        $data = [];
        $rules = [

            array(
                'field'
                => 'id_bahan_baku',
                'label' => 'ID Bahan Baku',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi !',
                    'alpha_numeric_spaces'  => '%s hanya angka 8-9 atau huruf A-Z'
                )
            ),
            array(
                'field' => 'nama_bahan_baku',
                'label' => 'Nama Bahan Baku',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi !',
                    'alpha'  => '%s hanya huruf A-Z'
                )
            ),
            array(
                'field' => 'satuan',
                'label' => 'Satuan',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi !',
                    'numeric'  => '%s hanya angka'
                )
            ),
            array(
                'field' => 'harga_satuan',
                'label' => 'Harga Satuan',
                'rules' => 'required',
                'errors' => array(
                    'required'              => '%s Wajib diisi bro !',
                    'numeric'  => '%s hanya angka'
                )
            ),

        ];

        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE) {
            $data = $this->layout();
            $data['sub_breadcrumbs_title'] = "Tambah Bahan Baku";
            $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

            $last_id = $this->model->get_last_row()[0]->id_bahan_baku;
            $id_number = (int) substr($last_id, 1, 3);
            $id_number++;
            $id_number = (string) $id_number;
            if (strlen($id_number) == 1)
                $id_string = 'B00' . $id_number;
            else if (strlen($id_number) == 2)
                $id_string = 'B0' . $id_number;
            else
                $id_string = 'B' .  $id_number;

            $data['model'] = $this->model;
            $data['id_string'] = $id_string;
            $this->load->view('bahan_create_view', $data);
        } else {
            $this->insert();
            redirect(site_url('bahan'));
        }
    }

    public function storeupdate()
    {
        $rules =
            [
                [
                    'field' => 'id_bahan_baku',
                    'label' => 'id_bahan_baku',
                    'rules' => 'required|alpha_numeric',
                    'errors' => [
                        'required' => "%s harus diisi",
                        'Alpha_numeric' => "%s Hanya boleh berisikan huruf & angka (tidak boleh spasi)",
                    ]
                ],


                [
                    'field' => 'nama_bahan_baku',
                    'label' => 'nama_bahan_baku',
                    'rules' => 'required|alpha',
                    'errors' => [
                        'required' => "%s harus diisi",
                        'alpha' => "%s hanya huruf a-z"
                    ]
                ],


                [
                    'field' => 'satuan',
                    'label' => 'satuan',
                    'rules' => 'required',
                    'errors' => [
                        'required' => "%s harus diisi",
                    ]
                ],
                [
                    'field' => 'harga_satuan',
                    'label' => 'harga_satuan',
                    'rules' => 'required|alpha_numeric',
                    'errors' => [
                        'required' => "%s harus diisi",
                        'Alpha_numeric' => "%s Hanya boleh berisikan huruf & angka (tidak boleh spasi)",
                    ]
                ]


            ];
        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == FALSE) {
            $data = $this->layout();
            $data['sub_breadcrumbs_title'] = "Ubah Bahan Baku";
            $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

            $this->load->view('bahan_create_view', $data);
        } else {

            $this->load->model('bahan_model');
            $this->minuman_model->update();
            redirect('bahan');
        }
    }

    public function create()
    {
        //belum implementasi
        if (isset($_POST['btnsubmit'])) {
            $this->model->id_bahan_baku = $_POST['id_bahan_baku'];
            $this->model->nama_bahan_baku = $_POST['nama_bahan_baku'];
            $this->model->satuan = $_POST['satuan'];
            $this->model->harga_satuan = $_POST['harga_satuan'];
            $this->model->insert();

            redirect('bahan');
        } else {
            $data = $this->layout();
            $data['sub_breadcrumbs_title'] = "Tambah Bahan Baku";
            $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

            $last_id = $this->model->get_last_row();

            if ($last_id->num_rows() == 0)
                $last_id = 'B000';
            else
                $last_id = $last_id->result()[0]->id_bahan_baku;

            $id_number = (int) substr($last_id, 1, 3);
            $id_number++;
            $id_number = (string) $id_number;
            if (strlen($id_number) == 1)
                $id_string = 'B00' . $id_number;
            else if (strlen($id_number) == 2)
                $id_string = 'B0' . $id_number;
            else
                $id_string = 'B' .  $id_number;

            $data['model'] = $this->model;
            $data['id_string'] = $id_string;
            $this->load->view('bahan_create_view', $data);
        }
    }

    public function update($id)
    {
        //belum implementasi
        if (isset($_POST['btnsubmit'])) {
            $this->model->id_bahan_baku = $_POST['id_bahan_baku'];
            $this->model->nama_bahan_baku = $_POST['nama_bahan_baku'];
            $this->model->satuan = $_POST['satuan'];
            $this->model->harga_satuan = $_POST['harga_satuan'];

            $this->model->update();
            redirect('bahan');
        } else {
            $query = $this->db->query("SELECT * FROM bahan_baku WHERE id_bahan_baku='$id'");
            if ($query->num_rows() > 0) {

                $row = $this->layout();

                $row['row'] = $query->row();

                $row['sub_breadcrumbs_title'] = "Ubah Bahan Baku";
                $row['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $row, TRUE);
                $this->load->view('bahan_update_view', $row);
            } else {
                echo "<script>alert('TIDAK KETEMU')</script>";
                $this->load->view('bahan_update_view', ['model' => $this->model]);
            }
        }
    }

    public function delete($id)
    {
        //menentukan kode yang akan di hapus
        $this->model->id = $id;
        //menghapus baris data didalam tabel barang
        $this->model->delete();
        //mengarahkan kembali kehalaman utama/index
        redirect('bahan');
    }
}

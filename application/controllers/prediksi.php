<?php
class Bonus extends CI_controller
{
    public $model = null;
    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION['role']))
		{
			return redirect(base_url() . 'login/admin');
        }
        
        //memuat model
        $this->load->model('bonus_model');
        $this->model = $this->bonus_model;
        //memuat library database
        $this->load->database();
    }

    public function index()
    {
      // handle view
      $data = $this->layout();
      $data['sub_breadcrumbs_title'] = "bonus";
      $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
      $data['rows'] = $this->model->read();
      $this->load->view('pa/bonus/prediksi_read_view', $data);
    }
}

<?php

class Topping extends CI_controller
{
	public $model = null;

	public function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['role']))
		{
			return redirect(base_url() . 'login/admin');
		}
		if(!isset($_SESSION['role']))
		{
			return redirect(base_url() . 'login/admin');
		}

		$this->load->model('topping_model');
		$this->load->model('kategori_model');
		$this->load->model('bahan_model');
		$this->load->library('Currency');
		$this->model = $this->topping_model;

		$this->load->database();
	}

	public function index()
	{
		$data = [];
		$this->read();
	}

	public function layout()
	{
		// Header
		$data['title'] = "Kinicheese Tea - Topping";
		$data['breadcrumbs_title'] = "Topping";
		$data['head'] = $this->load->view('layout/head', $data, TRUE);
		$data['header'] = $this->load->view('layout/header', NULL, TRUE);
		$data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);
		// $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

		// Footer
		$data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
		$data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
		$data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

		return $data;
	}

	public function create()
	{
		if (isset($_POST['btnsubmit'])) {
			$this->model->insert();
			redirect('topping');
		} else {
			$data = $this->layout();
			$data['sub_breadcrumbs_title'] = "Tambah Bahan Baku";
			$data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

			$last_id = $this->model->get_last_row();

			if ($last_id->num_rows() == 0)
				$last_id = 'B000';
			else
				$last_id = $last_id->result()[0]->id;

			$id_number = (int) substr($last_id, 1, 3);
			$id_number++;
			$id_number = (string) $id_number;
			if (strlen($id_number) == 1)
				$id_string = 'T00' . $id_number;
			else if (strlen($id_number) == 2)
				$id_string = 'T0' . $id_number;
			else
				$id_string = 'T' .  $id_number;

			$kategori = $this->kategori_model->read();
			$bahan_baku = $this->bahan_model->read();

			$data['model'] = $this->model;
			$data['id_string'] = $id_string;
			$data['kategori'] = $kategori;
			$data['bahan_baku'] = $bahan_baku;

			$this->load->view('topping_create_view', $data);
		}
	}

	public function read()
	{
		$data = $this->layout();
		$data['sub_breadcrumbs_title'] = "Lihat Topping";
		$data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
		$data['rows'] = $this->model->read();
		$this->load->view('topping_read_view', $data);
	}

	public function update($id)
	{
		if (isset($_POST['btnsubmit'])) {
			$this->model->id = $_POST['id'];
			$this->model->nama = $_POST['nama'];
			$this->model->harga = $_POST['harga'];

			$this->model->update();
			redirect('topping');
		} else {
			$query = $this->db->query("SELECT * FROM topping where id='$id'");
			if ($query->num_rows() > 0) {
				$row = $this->layout();
				$row['sub_breadcrumbs_title'] = "Ubah Topping";
				$row['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $row, TRUE);

				$row['row'] = $query->row();
				$last_id = $this->model->get_last_row()->row()->id;
				$id_number = (int) substr($last_id, 1, 3);
				$id_number++;
				$id_number = (string) $id_number;
				if (strlen($id_number) == 1)
					$id_string = 'T00' . $id_number;
				else if (strlen($id_number) == 2)
					$id_string = 'T0' . $id_number;
				else
					$id_string = 'T' .  $id_number;

				$row['id_string'] = $id_string;
				$row['kategori'] = $this->kategori_model->read();
				$this->load->view('topping_update_view', $row);
			} else {
				echo "<script>alert('TIDAK KETEMU')</script>";
				$this->load->view('topping_update_view', ['model' => $this->model]);
			}
		}
	}

	public function delete($id)
	{
		$this->model->id = $id;
		$this->model->delete();
		redirect('topping');
	}

	public function insert()
	{
		$this->load->model('topping_model');
		$this->topping_model->insert();
	}

	public function storecreate()
	{
		$data = [];
		$rules =
			[
				[
					'field' => 'id',
					'label' => 'id',
					'rules' => 'required|alpha_numeric',
					'errors' => [
						'required' => "%s harus diisi",
						'Alpha_numeric' => "%s Hanya boleh berisikan huruf & angka (tidak boleh spasi)",
					]
				],


				[
					'field' => 'nama',
					'label' => 'nama',
					'rules' => 'required',
					'errors' => [
						'required' => "%s harus diisi",
					]
				],


				[
					'field' => 'harga',
					'label' => 'harga',
					'rules' => 'required',
					'errors' => [
						'required' => "%s harus diisi",
					]
				],


			];

		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() == False) {
			$data = $this->layout();
			$data['sub_breadcrumbs_title'] = "Tambah Topping";
			$data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

			$last_id = $this->model->get_last_row()[0]->id;
			$id_number = (int) substr($last_id, 1, 3);
			$id_number++;
			$id_number = (string) $id_number;
			if (strlen($id_number) == 1)
				$id_string = 'M00' . $id_number;
			else if (strlen($id_number) == 2)
				$id_string = 'M0' . $id_number;
			else
				$id_string = 'M' .  $id_number;

			$data['id_string'] = $id_string;
			$data['model'] = $this->model;
			$data['kategori'] = $this->kategori_model->read();

			$this->load->view('topping_create_view', $data);
		} else {

			$this->insert();
			redirect(site_url('topping'));
		}
	}

	public function storeupdate()
	{
		$rules =
			[
				[
					'field' => 'id',
					'label' => 'id',
					'rules' => 'required|alpha_numeric',
					'errors' => [
						'required' => "%s harus diisi",
						'Alpha_numeric' => "%s Hanya boleh berisikan huruf & angka (tidak boleh spasi)",
					]
				],


				[
					'field' => 'nama',
					'label' => 'nama',
					'rules' => 'required|alpha',
					'errors' => [
						'required' => "%s harus diisi",
						'alpha' => "%s hanya huruf a-z"
					]
				],


				[
					'field' => 'harga',
					'label' => 'harga',
					'rules' => 'required',
					'errors' => [
						'required' => "%s harus diisi",
					]
				],


			];
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() == False) {

			$data = $this->layout();
			$data['sub_breadcrumbs_title'] = "Ubah Topping";
			$data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);



			$this->load->view('topping_create_view', $data);
		} else {

			$this->load->model('topping_model');
			$this->topping_model->update();
			redirect('topping');
		}
	}
}

<?php

class Pegawai extends CI_controller
{
	public $model = null;

	public function __construct()
	{
		parent::__construct();
		if(!isset($_SESSION['role']))
		{
			return redirect(base_url() . 'login/admin');
		}

		$this->load->model('pegawai_model');
		$this->model = $this->pegawai_model;
		$this->load->library('Currency');
		$this->load->helper(array('form', 'url'));

		$this->load->library('form_validation');

		$this->load->database();
	}

	public function index()
	{
		$this->read();
	}

	public function layout()
	{
		// Header
		$data['title'] = "Kinicheese Tea - Pegawai";
		$data['breadcrumbs_title'] = "Pegawai";
		$data['head'] = $this->load->view('layout/head', $data, TRUE);
		$data['header'] = $this->load->view('layout/header', NULL, TRUE);
		$data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);
		$data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

		// Footer
		$data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
		$data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
		$data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

		return $data;
	}

	public function create()
	{
		if (isset($_POST['btnsubmit'])) 
		{
			$rules = [

				array(
						'field' => 'id_pegawai',
						'label' => 'ID Pegawai',
						'rules' => 'required',
						'errors' => array(
								'required'              => '%s Wajib diisi !',
								'alpha_numeric'  => '%s hanya angka dan huruf'
						)
				),
				array(
						'field' => 'nama_pegawai',
						'label' => 'Nama Peawai',
						'rules' => 'required',
						'errors' => array(
								'required'              => '%s Wajib diisi !',
								'alpha'  => '%s hanya huruf'
						)
				),
				array(
					'field' => 'alamat',
					'label' => 'Alamat',
					'rules' => 'required',
					'errors' => array(
							'required'              => '%s Wajib diisi !',
							'alpha_numeric_spaces'  => '%s hanya angka dan huruf dan spasi'
					)
				),
				array(
					'field' => 'no_telp',
					'label' => 'Nomor',
					'rules' => 'required',
					'errors' => array(
							'required'              => '%s Wajib diisi !',
							'numeric'  => '%s hanya angka'
					)
			),

			];

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run() == 1)
			{
				$this->model->id_pegawai = $_POST['id_pegawai'];
				$this->model->nama_pegawai = $_POST['nama_pegawai'];
				$this->model->alamat = $_POST['alamat'];
				$this->model->no_telp = $_POST['no_telp'];

				$this->model->insert();
				redirect('pegawai');
			}			
		} 
		else 
		{
			$data = $this->layout();
			$data['sub_breadcrumbs_title'] = "Tambah Pegawai";
			$data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

			$last_id = $this->model->db->query("SELECT * FROM pegawai ORDER BY id_pegawai DESC LIMIT 1");

			if ($last_id->num_rows() == 0)
				$last_id = 'P000';
			else
				$last_id = $last_id->result()[0]->id_pegawai;

			$id_number = (int) substr($last_id, 1, 3);
			$id_number++;
			$id_number = (string) $id_number;
			if (strlen($id_number) == 1)
				$id_string = 'P00' . $id_number;
			else if (strlen($id_number) == 2)
				$id_string = 'P0' . $id_number;
			else
				$id_string = 'P' .  $id_number;

			$data['model'] = $this->model;
			$data['id_string'] = $id_string;
			$data['jabatans'] = $this->db->get('jabatan')->result();

			$this->load->view('pegawai_create_view', $data);
		}
	}

	public function read()
	{
		$data = $this->layout();
		$data['sub_breadcrumbs_title'] = "Lihat Pegawai";
		$data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

		$data['rows'] = $this->model->read();
		$this->load->view('pegawai_read_view', $data);
	}

	public function update($id)
	{
		if (isset($_POST['btnsubmit'])) 
		{
			$rules = [

				array(
						'field' => 'id_pegawai',
						'label' => 'ID Pegawai',
						'rules' => 'required',
						'errors' => array(
								'required'              => '%s Wajib diisi !',
								'alpha_numeric'  => '%s hanya angka dan huruf'
						)
				),
				array(
						'field' => 'nama_pegawai',
						'label' => 'Nama Peawai',
						'rules' => 'required',
						'errors' => array(
								'required'              => '%s Wajib diisi !',
								'alpha_numeric_spaces'  => '%s hanya angka dan huruf'
						)
				),
				array(
					'field' => 'alamat',
					'label' => 'Alamat',
					'rules' => 'required',
					'errors' => array(
							'required'              => '%s Wajib diisi !',
							'alpha_numeric_spaces'  => '%s hanya angka dan huruf dan spasi'
					)
				),
				array(
					'field' => 'no_telp',
					'label' => 'No Telp',
					'rules' => 'required',
					'errors' => array(
							'required'              => '%s Wajib diisi !',
							'numeric'  => '%s hanya angka'
					)
			),

			];

			$this->form_validation->set_rules($rules);

			if($this->form_validation->run())
			{
				$this->model->id_pegawai = $_POST['id_pegawai'];
				$this->model->nama_pegawai = $_POST['nama_pegawai'];
				$this->model->alamat = $_POST['alamat'];
				$this->model->no_telp = $_POST['no_telp'];

				$this->model->update();
				redirect('pegawai');
			}
		} 
		else 
		{
			$query = $this->db->query("SELECT * FROM pegawai where id_pegawai='$id'");
			if ($query->num_rows() > 0) {
				$row = $this->layout();
				$row['sub_breadcrumbs_title'] = "Ubah Pegawai";
				$row['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $row, TRUE);
				$row['jabatans'] = $this->db->get('jabatan')->result();

				$row['row'] = $query->row();

				$this->load->view('pegawai_update_view', $row);
			} else {
				echo "<script>alert('TIDAK KETEMU')</script>";
				$this->load->view('pegawai_update_view', ['model' => $this->model]);
			}
		}
	}

	public function delete($id)
	{
		$this->model->db->query('SET FOREIGN_KEY_CHECKS=0');
		$this->model->id_pegawai = $id;
		$this->model->delete();
		$this->model->db->query('SET FOREIGN_KEY_CHECKS=1');
		redirect('pegawai');
	}

	public function storecreate()
	{
		$this->load->model('pegawai_model');
		$this->pegawai_model->insert();
		redirect('pegawai');
	}

	public function storeupdate()
	{
		$this->load->model('pegawai_model');
			$this->pegawai_model->update();
			redirect('pegawai');
	}
}

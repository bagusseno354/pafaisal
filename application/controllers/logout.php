<?php
class Logout extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        //memuat library database
        $this->load->database();
    }

    public function index()
    {
      session_destroy();

      return redirect('/');
    }
}

<?php
class Login extends CI_controller
{
    public function __construct()
    {
        parent::__construct();
        //memuat library database
        $this->load->database();
    }

    public function index()
    {
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Login";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
        $this->load->view('pa/login_select', $data);
    }

    public function admin()
    {
        // request
        if(isset($_POST['btnsubmit']))
        {
            $this->db->where('username', $this->input->post('username'));
            $this->db->where('password', $this->input->post('password'));
            $q = $this->db->get('user')->result();
            
            if(count($q) > 0)
            {
                session_start();
                $_SESSION['role'] = 'Administrator';
                $_SESSION['username'] = $this->input->post('username');
                $_SESSION['id_pegawai'] = $q[0]->id_pegawai;
    
                return redirect('/');
            }
            else
            {
                $this->session->set_flashdata('status', 'error');
            }
        }

        // view
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Login";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
        $this->load->view('pa/login', $data);
    }

    public function pegawai()
    {
        // request
        if(isset($_POST['btnsubmit']))
        {
            $this->db->where('username', $this->input->post('username'));
            $this->db->where('password', $this->input->post('password'));
            $q = $this->db->get('pegawai')->result();
            
            if(count($q) > 0)
            {
                session_start();
                $_SESSION['role'] = 'Pegawai';
                $_SESSION['username'] = $this->input->post('username');
                $_SESSION['id_pegawai'] = $q[0]->id_pegawai;
    
                return redirect('/');
            }
            else
            {
                $this->session->set_flashdata('status', 'error');
            }
        }

        // view
        $data = $this->layout();
        $data['sub_breadcrumbs_title'] = "Login";
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
        $this->load->view('pa/login', $data);
    }

    public function layout()
    {
        // Header
        $data['title'] = "Kinicheese Tea - Izin";
        $data['breadcrumbs_title'] = "Izin";
        $data['head'] = $this->load->view('layout/head', $data, TRUE);
        $data['header'] = $this->load->view('layout/header', NULL, TRUE);
        $data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);


        // Footer
        $data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
        $data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

        return $data;
    }
}

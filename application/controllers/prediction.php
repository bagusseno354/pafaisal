<?php
class Bonus extends CI_controller
{
    public $model = null;
    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION['role']))
		{
			return redirect(base_url() . 'login/admin');
        }
        
        //memuat model
        $this->load->model('bonus_model');
        $this->model = $this->bonus_model;
        //memuat library database
        $this->load->database();
    }

    public function index()
    {      

      // handle view  
      $data = $this->layout();
      $data['sub_breadcrumbs_title'] = "prediction";
      $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
      $data['rows'] = $this->db->get_where('bonus', array('id' => $id))->result();
      $this->load->view('pa/prediction_read_view', $data);
    }

       
    public function layout()
    {
        // Header
        $data['title'] = "Kinicheese Tea - prediction";
        $data['breadcrumbs_title'] = "prediction";
        $data['head'] = $this->load->view('layout/head', $data, TRUE);
        $data['header'] = $this->load->view('layout/header', NULL, TRUE);
        $data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);


        // Footer
        $data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
        $data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

        return $data;
    }

    public function data()
    {
        // return $this->db->get()
    }
}

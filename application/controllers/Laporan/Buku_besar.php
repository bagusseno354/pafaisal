<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Buku_besar extends CI_Controller
{

    public $model = null;

    public function __construct()
    {
        parent::__construct();

        // Jurnal Umum Model
        $this->load->model('buku_besar_model');
        $this->load->library('Currency');
        $this->load->library('Date');
    }
    public function index()
    {
        $this->read();
    }

    public function layout()
    {
        $akun = "";

        if (isset($_GET['kode_akun'])) {

            switch ($_GET['kode_akun']) {

                case 111:
                    $akun = 'Kas';
                    break;
                case 113:
                    $akun = 'Pembelian';
                    break;
                case 411:
                    $akun = 'Penjualan';
                    break;
                case 512:
                    $akun = 'Retur';
                    break;
                case 513:
                    $akun = 'Gaji Karyawan';
                    break;
                case 514:
                    $akun = 'Biaya Angkut';
                    break;
                case 112:
                    $akun = 'Persediaan Barang Dagang';
                    break;
                case 43:
                    $akun = 'Pendapatan Bunga';
                    break;
            }
        }

        // Header
        $data['title'] = "Kinicheese Tea - Buku Besar $akun";
        $data['breadcrumbs_title'] = "Buku Besar";
        $data['sub_breadcrumbs_title'] = "Buku Besar $akun";

        $data['head'] = $this->load->view('layout/head', $data, TRUE);
        $data['header'] = $this->load->view('layout/header', NULL, TRUE);
        $data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

        // Footer
        $data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
        $data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

        return $data;
    }

    public function read()
    {
        if (isset($_POST['keterangan'], $_POST['id_transaksi'])) {

            $this->buku_besar_model->update_keterangan($_POST['id_transaksi'], $_POST['keterangan']);
        }

        $data = $this->layout();

        $data['kas'] = [];
        $data['saldo_akhir']['saldo_akhir_debit'] = 0;
        $data['saldo_akhir']['saldo_akhir_kredit'] = 0;

        // Data
        if (isset($_GET['bulan'])) {
            $data['kas'] = $this->buku_besar_model->read();
            $data['saldo_akhir'] = $this->buku_besar_model->read_saldo_akhir($_GET['kode_akun']);
        }

        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

        // $data['rows'] = $this->model->read();
        $this->load->view('buku_besar_read_view', $data);
    }

    public function update($id)
    {
    }

    public function delete($id)
    {
    }
}

/* End of file Buku_besar.php */

<?php
class Input_beban extends CI_controller
{
    public $model = null;
    public function __construct()
    {
        parent::__construct();

        $this->load->model('jurnal_umum_model');

        $this->load->database();
    }

    public function index()
    {

        $this->create();
    }

    public function layout()
    {
        // Header
        $data['title'] = "Kinicheese Tea - Input Beban";
        $data['breadcrumbs_title'] = "Input Beban";
        $data['head'] = $this->load->view('layout/head', $data, TRUE);
        $data['header'] = $this->load->view('layout/header', NULL, TRUE);
        $data['sidebar_left'] = $this->load->view('layout/sidebar_left', NULL, TRUE);
        $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

        // Footer
        $data['sidebar_right'] = $this->load->view('layout/sidebar_right', NULL, TRUE);
        $data['footer'] = $this->load->view('layout/footer', NULL, TRUE);
        $data['scripts'] = $this->load->view('layout/scripts', NULL, TRUE);

        return $data;
    }

    public function insert()
    {
        // get last transaction id
        $last_jurnal_id = $this->db->query("SELECT * FROM jurnal_umum ORDER BY id_transaksi DESC LIMIT 1");
        if ($last_jurnal_id->num_rows() > 0)
            $last_jurnal_id = $last_jurnal_id->result()[0]->id_transaksi;
        else
            $last_jurnal_id = 0;

        $jurnal_id = $last_jurnal_id + 1;
        $kode_akun = $this->input->post('kode_akun');

        // jurnal umum
        $jurnal_umum_d = [
            'id_transaksi' => $jurnal_id,
            'kode_akun' => $kode_akun,
            'posisi_d_c' => 'd',
            'nominal' => $this->input->post('nominal'),
            'transaksi' => '',
            'tgl_jurnal' => $this->input->post('tanggal')
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_d);

        // jurnal umum
        $jurnal_umum_c = [
            'id_transaksi' => $jurnal_id,
            'kode_akun' => 111,
            'posisi_d_c' => 'c',
            'nominal' => $this->input->post('nominal'),
            'transaksi' => '',
            'tgl_jurnal' => $this->input->post('tanggal')
        ];

        $this->db->insert('jurnal_umum', $jurnal_umum_c);
    }

    public function create()
    {
        if (isset($_POST['btnsubmit'])) {
            $this->model->insert();
            redirect('laporan/input_pendapatan');
        } else {
            $data = $this->layout();
            $data['sub_breadcrumbs_title'] = "Tambah pendapatan bunga";
            $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);
            $data['model'] = $this->model;

            $this->load->view('beban_create_view', $data);
        }
    }

    public function storecreate()
    {
        $rules =

            [
                [
                    'field' => 'tanggal',
                    'label' => 'Tanggal',
                    'rules' => 'required|date',
                    'errors' => [
                        'required' => "%s harus diisi",
                    ]
                ],
                [
                    'field' => 'jenis_pendapatan',
                ],
                [
                    'field' => 'nominal',
                    'label' => 'Nominal',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => "%s harus diisi",
                    ]
                ],


            ];



        $this->form_validation->set_rules($rules);

        if ($this->form_validation->run() == False) {
            $data = $this->layout();
            $data['sub_breadcrumbs_title'] = "Input pendapatan";
            $data['breadcrumbs'] = $this->load->view('layout/breadcrumbs', $data, TRUE);

            redirect(site_url('laporan/input_pengeluaran'));
        } else {

            $this->insert();
            redirect(site_url('laporan/laba_rugi'));
        }
    }
}

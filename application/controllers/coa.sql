-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 10, 2020 at 07:48 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kinicheesetea1`
--

-- --------------------------------------------------------

--
-- Table structure for table `coa`
--

CREATE TABLE `coa` (
  `kode_akun` int(11) NOT NULL,
  `nama_akun` varchar(100) NOT NULL,
  `header_kode_akun` int(11) NOT NULL,
  `posisi_d_c` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa`
--

INSERT INTO `coa` (`kode_akun`, `nama_akun`, `header_kode_akun`, `posisi_d_c`) VALUES
(1, 'Aktiva', 0, 'd'),
(2, 'Kewajiban', 0, 'c'),
(3, 'Modal', 0, 'c'),
(4, 'Pendapatan', 0, 'c'),
(5, 'Beban', 0, 'd'),
(11, 'Aktiva Lancar', 1, 'd'),
(12, 'Aktiva Tetap', 1, 'd'),
(21, 'Kewajiban Lancar', 2, 'c'),
(22, 'Kewajiban Jangka Panjang', 2, 'c'),
(31, 'Modal Pemilik', 3, 'c'),
(41, 'Pendapatan Operasional', 4, 'c'),
(42, 'Pendapatan Non Operasional', 4, 'c'),
(43, 'Pendapatan bunga', 4, 'd'),
(51, 'Beban Operasional', 5, 'd'),
(52, 'Beban Non Operasional', 5, 'd'),
(111, 'Kas', 11, 'd'),
(112, 'Persediaan Barang Dagang', 11, 'd'),
(113, 'Pembelian', 11, 'd'),
(411, 'Penjualan', 41, 'c'),
(412, 'Harga Pokok Penjualan', 41, 'd'),
(511, 'Beban Administrasi dan Umum', 51, 'd'),
(512, 'Retur', 51, 'c'),
(513, 'Gaji karyawan', 51, 'c'),
(514, 'Biaya angkut', 51, 'c');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`kode_akun`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <?= $sidebar_left ?>

        <!-- START CONTENT -->
        <section id="content">

            <?= $breadcrumbs ?>

            <!--Basic Form-->
            <div id="basic-form" class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <div class="row">
                                <form action="storecreate" method="POST" class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>ID Penjualan</label>
                                            <input required type="text" value='<?php echo $id_string ?>' name="id_penjualan" class="form-control"><span class="text-danger"><?= form_error('id_penjualan') ?></span>
                                        </div>
                                    </div>
                                    <div id="tambah_field">
                                        <div class="row">
                                            <div class="input-field col s4">
                                                <select required style="display:block" class='form-control' id='id_minuman' name='id_minuman[]'>
                                                    <?php
                                                    foreach ($minuman as $k => $v) {

                                                        echo "<option harga=$v->harga value='$v->id_minum'>$v->nama_minum</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <label>Minuman</label>
                                            </div>
                                            <div class="input-field col s4">
                                                <label>Jumlah </label>
                                                <input required type="number" name="jumlah[]" class="form-control jumlah"><span class="text-danger"><?= form_error('harga') ?></span>
                                            </div>
                                            <div class="input-field col s4">
                                                <select required style="display:block" class='form-control' id='id_topping' name='id_topping[]'>
                                                    <option harga='0' value='none'>None</option>
                                                    <?php
                                                    foreach ($topping as $k => $v) {

                                                        echo "<option harga=$v->harga value='$v->id'>$v->nama</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <label>Topping</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button class="cyan waves-effect waves-light btn" id="tambah">Tambah</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Total Harga</label>
                                            <input required type="text" id="totalharga" name="totalharga" class="form-control" value=""><span class="text-danger"><?= form_error('harga') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>ID Pegawai </label>
                                            <select required class='form-control' id='id_pegawai' name='id_pegawai'>
                                                <?php
                                                foreach ($pegawai as $k => $v) {
                                                    echo "<option value='$v->id_pegawai'>$v->nama_pegawai</option>\n";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Total bayar</label>
                                            <input required type="number" id="totalbayar" name="totalbayar" class="form-control" value=""><span class="text-danger"><?= form_error('harga') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button id="submit" type="submit" name="btnsubmit" class="cyan waves-effect waves-light btn">Simpan<i class="mdi-content-send right"></i></button>
                                            </button>
                                            <a href="<?= base_url() ?>penjualan" class="btn waves-effect waves-light red"><i class=" mdi-content-clear"></i></a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--start container-->
            <div class="container">

            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <?= $sidebar_right ?>

    </div>
    <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $scripts ?>

<script>
    $bahan_baku_html = $('#tambah_field').html()
    $hapus_html = '<div class="input-field col l2">' +
        '<button class="cyan waves-effect waves-light btn block hapus" style="width:100%">Hapus</button>' +
        '</div>';

    $('#tambah').click((e) => {
        e.preventDefault()
        $('#tambah_field').append($bahan_baku_html);
        $('#tambah_field').children().last().append($hapus_html)

        $('.hapus').click((e) => {
            e.preventDefault()
            $(e.currentTarget).parent().parent().remove();

            let totalHarga = 0;
            $('#tambah_field option:selected').each((k, v) => {
                let jumlah = $(v).closest('.row').find('.jumlah').val();
                totalHarga += parseInt($(v).attr('harga')) * jumlah;        })

            $('#totalharga').val(totalHarga)
        })

        $('#tambah_field select').off('change').change(() =>
        {   
            let totalHarga = 0;
            $('#tambah_field option:selected').each((k, v) => {
                let jumlah = $(v).closest('.row').find('.jumlah').val();
                totalHarga += parseInt($(v).attr('harga')) * jumlah;                                     
            })

            $('#totalharga').val(totalHarga)
        })

        $('#tambah_field input').off('keyup').keyup(() =>
        {   
            let totalHarga = 0;
            $('#tambah_field option:selected').each((k, v) => {
                let jumlah = $(v).closest('.row').find('.jumlah').val();
                totalHarga += parseInt($(v).attr('harga')) * jumlah;        })

            $('#totalharga').val(totalHarga)
        })
    });

    $('#tambah_field select').off('change').change(() =>
    {   
        let totalHarga = 0;
        $('#tambah_field option:selected').each((k, v) => {
            let jumlah = $(v).closest('.row').find('.jumlah').val();
            totalHarga += parseInt($(v).attr('harga')) * jumlah;                                     
        })

        $('#totalharga').val(totalHarga)
    })

    $('#tambah_field input').off('keyup').keyup(() =>
    {   
        let totalHarga = 0;
        $('#tambah_field option:selected').each((k, v) => {
            let jumlah = $(v).closest('.row').find('.jumlah').val();
            totalHarga += parseInt($(v).attr('harga')) * jumlah;        })

        $('#totalharga').val(totalHarga)
    })
</script>
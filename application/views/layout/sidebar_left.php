            <!-- START LEFT SIDEBAR NAV-->            
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                    <li class="user-details cyan darken-2">
                        <div class="row">
                            <div class="col col s4 m4 l4">
                                <img src="<?= base_url() ?>assets/images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                            </div>
                            <div class="col col s8 m8 l8">
                                <ul id="profile-dropdown" class="dropdown-content">
                                    <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                                    </li>
                                    <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                                    </li>
                                    <li><a href="<?= base_url() ?>logout"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                                    </li>
                                </ul>
                                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?php echo $_SESSION['username'] ?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                                <p class="user-roal"><?php echo $_SESSION['role'] ?></p>
                            </div>
                        </div>
                    </li>
                    <li class="bold"><a href="<?= base_url() ?>dashboard" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i>Dashboard</a>
                    </li>
                    <li class="no-padding">
                        <ul class="collapsible collapsible-accordion">
                            
                            <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-image-palette"></i>COA</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>coa">Lihat COA</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>coa/create">Tambah COA</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-border-all"></i> Kategori</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>kategori">Lihat Kategori</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>kategori/create">Tambah Kategori</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-social-pages"></i>Minuman</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>minuman">Lihat Minuman</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>minuman/create">Tambah Minuman</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-social-pages"></i>Topping</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>topping">Lihat Topping</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>topping/create">Tambah Topping</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a class="collapsible-header  waves-effect waves-cyan"><i class="mdi-editor-insert-comment"></i>Pegawai</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>pegawai">Lihat Pegawai</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>pegawai/create">Tambah Pegawai</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            
                            
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Input Manual</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>laporan/input_pendapatan/">Input Pendapatan</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>laporan/input_beban/">Input Beban</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>   
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Izin</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <?php if($_SESSION['role'] == 'Pegawai') { ?>
                                        <li><a href="<?= base_url() ?>izin/pengajuan/">Ajukan Izin</a>
                                        </li>
                                        <?php } ?>
                                        <li><a href="<?= base_url() ?>izin">Riwayat Izin</a>
                                        </li>
                                        <?php if($_SESSION['role'] == 'Administrator') { ?>
                                        <li><a href="<?= base_url() ?>izin/persetujuan/">Persetujuan Izin</a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>  
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Presensi</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>presensi/tapping/">Tapping</a>
                                        </li>
                                        <?php if($_SESSION['role'] == 'Administrator') { ?>
                                        <li><a href="<?= base_url() ?>presensi/setting/">Setting</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>presensi/manage/">Manage</a>
                                        </li>
                                        <?php } ?>
                                        <li><a href="<?= base_url() ?>presensi/riwayat/">Riwayat</a>
                                        </li>
                                    </ul>
                                </div>
                            </li> 
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Piutang</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <?php if($_SESSION['role'] == 'Pegawai') { ?>
                                        <li><a href="<?= base_url() ?>piutang/Pengajuan/">Pengajuan</a>
                                        </li>
                                        <?php } ?>
                                        <?php if($_SESSION['role'] == 'Administrator') { ?>
                                        <li><a href="<?= base_url() ?>piutang/Persetujuan/">Persetujuan</a>
                                        </li>
                                        <?php } ?>
                                        <li><a href="<?= base_url() ?>piutang">Riwayat</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Tunjangan</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>Tunjangan/Tambah/">Tambah</a>
                                        </li>
                                        <?php if($_SESSION['role'] == 'Administrator') { ?>
                                        <li><a href="<?= base_url() ?>Tunjangan/Setting/">Setting</a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </li>
                            <?php if($_SESSION['role'] == 'Administrator') { ?>
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Jabatan</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>Jabatan/Tambah/">Tambah</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>Jabatan/Manage/">Manage</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Bonus</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>Bonus/Setting/">Setting</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-swap-vert-circle"></i>Gaji</a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="<?= base_url() ?>Gaji/riwayat">Riwayat</a>
                                        </li>
                                        <li><a href="<?= base_url() ?>Gaji/list">Konfirmasi</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <?php } ?>
                        </li>
                        <li class="li-hover">
                            <div class="divider"></div>
                        </li>
                        <li class="li-hover">
                            <p class="ultra-small margin more-text">MORE</p>
                        </li>
                        
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-image-grid-on"></i>Penjualan</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="<?= base_url() ?>penjualan">Lihat Penjualan</a>
                                    </li>
                                    <li><a href="<?= base_url() ?>penjualan/create">Tambah Penjualan</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i>Laporan</a>
                            <div class="collapsible-body" id="menu-laporan">
                                <ul>
                                    <li><a href="<?= base_url() ?>laporan/jurnal_umum">Jurnal Umum</a>
                                    </li>
                                    <li><a href="<?= base_url() ?>laporan/buku_besar">Buku Besar</a>
                                    </li>
                                    <li><a href="<?= base_url() ?>laporan/laba_rugi">Laba Rugi</a>
                                    </li>           
                                    <li><a href="<?= base_url() ?>laporan/laporan_penjualan">Laporan Penjualan</a>
                                    </li>
                                    <li><a href="<?= base_url() ?>laporan/laporan_pendapatan_beban">Laporan Beban & Pen...</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <li class="li-hover">
                        <div class="divider"></div>
                    </li>
                </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
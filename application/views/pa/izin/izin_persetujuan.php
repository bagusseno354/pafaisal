<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
	<div id="loader"></div>
	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
	<!-- START WRAPPER -->
	<div class="wrapper">

		<?= $sidebar_left ?>

		<!-- START CONTENT -->
		<section id="content">

			<?= $breadcrumbs ?>

			<!--start container-->
			<div class="container">

				<div class="divider"></div>
				<!--End Tambah Button-->

				<!--DataTables-->
				<div id="table-datatables">
					<div class="row">
						<div class="col s4 m8 l12">

							<table id="data-table-simple" class="responsive-table display excel-table" cellspacing="0" style="text-align: center">
								<thead>
									<tr>
										<th width="10%">ID Izin</th>
										<th width="10%">Pegawai</th>
										<th width="10%">Tanggal Mulai</th>
										<th width="10%">Tanggal Selesai</th>
										<th width="10%">Keterangan</th>
										<th width="10%">Status</th>
										<th width="10%">Action</th>
									</tr>
								</thead>

								<tbody>
									<?php
									foreach ($rows as $row) {
									?>
										<tr>
											<td><?php echo $row->id; ?></td>
											<td><?php echo $row->nama_pegawai; ?></td>
											<td><?php echo $row->tanggal_mulai; ?></td>
											<td><?php echo $row->tanggal_selesai; ?></td>
											<td><?php echo $row->keterangan; ?></td>
											<td><?php echo $row->status; ?></td>
											<td>
												<a href="<?php echo base_url()  ?>izin/izinkan/<?php echo $row->id; ?>">Izinkan</a> | 
												<a href="<?php echo base_url()  ?>izin/tolak/<?php echo $row->id; ?>">Tolak</a>
											</td>
										</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End DataTables -->

			</div>
			<!--end container-->
		</section>
		<!-- END CONTENT -->

		<?= $sidebar_right ?>

	</div>
	<!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>
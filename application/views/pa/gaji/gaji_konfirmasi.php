<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <?= $sidebar_left ?>

        <!-- START CONTENT -->
        <section id="content">

            <?= $breadcrumbs ?>

            <!--Basic Form-->
            <div id="basic-form" class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <div class="row">
                            <?php if (!$exist) { ?>
                                <form action="" method="POST" class="col s12">
                                    <div class="row">
                                        <div class="col s12">
                                            <label>ID pegawai</label>
                                            <input readonly type="text" name="id_pegawai" class="form-control" value="<?php echo $pegawai->id_pegawai ?>"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Nama pegawai</label>
                                            <input readonly type="text" name="nama" class="form-control" value="<?php echo $pegawai->nama_pegawai ?>"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Gaji Pokok</label>
                                            <input type="number" name="gaji_pokok" readonly value="<?php echo $gaji_pokok ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Total Telat (dalam menit)</label>
                                            <input type="number" name="total_denda" readonly value="<?php echo $total_telat ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Total Bolos</label>
                                            <input type="number" name="total_denda" readonly value="<?php echo $total_bolos ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Total Denda</label>
                                            <input type="number" name="total_denda" readonly value="<?php echo $total_denda ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Tunjangan</label>
                                            <?php $totalTunjangan = 0; foreach($tunjangans as $t) { $totalTunjangan += $t->nominal?>
                                            <dl><?php echo $t->nama ?>: <?php echo $t->nominal ?></dl>
                                            <?php } ?>
                                            <dl>
                                            Total: <?php echo $totalTunjangan ?>
                                            <input type="hidden" name="total_tunjangan" value="<?php echo $totalTunjangan ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Bonus</label>
                                            <input readonly type="number" name="bonus" id="jam_tiba_mulai" class="form-control" value="<?php echo $bonus ?>"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Utang</label>
                                            <?php $totalUtang = 0; foreach($utangs as $t) { $totalUtang += $t->nominal?>
                                            <dl>Utang <?php echo $t->tanggal ?>: <?php echo $t->nominal ?></dl>
                                            <?php } ?>
                                            <dl>
                                            Total: <?php echo $totalUtang ?>
                                            <input type="hidden" name="total_utang" value="<?php echo $totalUtang ?>">
                                        </div>                                   
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Total Gaji</label>
                                            <input type="number" name="total_gaji" readonly value="<?php echo $gaji_pokok + $totalTunjangan + $bonus - $total_denda - $totalUtang ?>">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <button type="submit" name="btnsubmit" class="cyan waves-effect waves-light btn">Konfirmasi<i class="mdi-content-send right"></i></button>
                                            </button>
                                            <a href="<?= base_url() ?>laporan/input_pendapatan" class="btn waves-effect waves-light red"><i class=" mdi-content-clear"></i></a>
                                        </div>
                                    </div>
                                            <?php } else { ?>
                                            Maaf, pegawai dengan nama <?php echo $pegawai->nama_pegawai ?> sudah dikonfirmasi bulan <?php echo $month ?> tahun <?php echo $year ?>
                                            <?php } ?>
                                            
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--start container-->
            <div class="container">

            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <?= $sidebar_right ?>

    </div>
    <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>

<script>
    $(document).ready(() => {

        $("#tanggal-mulai").change(() => {

            var date = new Date($("#tanggal-mulai").val())
            date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
            $("#tanggal-mulai").val(date)

            console.log($("#tanggal-mulai").val())
        })

        $("#tanggal-selesai").change(() => {

            var date = new Date($("#tanggal-selesai").val())
            date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
            $("#tanggal-selesai").val(date)

            console.log($("#tanggal-selesai").val())
        })
    })
</script>
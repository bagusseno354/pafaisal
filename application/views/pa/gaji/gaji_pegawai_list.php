<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
	<div id="loader"></div>
	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
	<!-- START WRAPPER -->
	<div class="wrapper">

		<?= $sidebar_left ?>

		<!-- START CONTENT -->
		<section id="content">

			<?= $breadcrumbs ?>

			<!--start container-->
			<div class="container">

				<!--Tambah Button-->
				<div class="divider"></div>

				<a href="<?= base_url() ?>tunjangan/tambah">
					<button class="cyan waves-effect waves-light btn left" style="width: 100%; height:3rem; margin: 1rem auto">
						<i class="mdi-content-add left"></i>TAMBAH
					</button>
				</a>

				<div class="divider"></div>
				<!--End Tambah Button-->

				<!--DataTables-->
				<div id="table-datatables">
					<div class="row">
						<div class="col s4 m8 l12">

							<table id="data-table-simple" class="responsive-table display excel-table" cellspacing="0" style="text-align: center">
								<thead>
									<tr>
										<th width="10%">ID Pegawai</th>
										<th width="50%">Nama</th>
										<th width="10%">Action</th>
									</tr>
								</thead>

								<tbody>
									<?php
									foreach ($rows as $row) {
									?>
										<tr class='row-pegawai'>
											<td><?php echo $row->id_pegawai; ?></td>
											<td><?php echo $row->nama_pegawai; ?></td>
											<td>
											<input class="month" type="number" max=31 min=0 placeholder="Bulan">
											<input class="year" type="number" max=31 min=0 placeholder="Tahun">
												<a class='gaji-pegawai' id-pegawai=<?php echo $row->id_pegawai ?>>Gaji</a>
											</td>
										</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End DataTables -->

			</div>
			<!--end container-->
		</section>
		<!-- END CONTENT -->

		<?= $sidebar_right ?>

	</div>
	<!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>

<script>
	$('.gaji-pegawai').click((e) =>
	{
		location.replace("<?php echo base_url() . 'gaji/konfirmasi/' ?>" + $(e.currentTarget).attr('id-pegawai') + "/" + $(e.currentTarget).siblings('.month').val() + "/" + $(e.currentTarget).siblings('.year').val());
	})
</script>
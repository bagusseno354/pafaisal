<html>
    <body>
        <center>KINI CHEESETEA - SLIP GAJI</center>
        <p>
            Nama: <?php echo $gaji->nama_pegawai ?>
            <br>
            Waktu: <?php echo $gaji->waktu_generate ?>
        </p>  
        <table>              
            <tr>
                <td>Nama</td>
                <td>Jumlah</td>
            </tr>
            <tr>
                <td>Gaji pokok</td>
                <td>+<?php echo $gaji_pokok - $total_denda; $total = $gaji_pokok - $total_denda; ?></td>
            </tr>
            <?php
                foreach($tunjangans as $tunjangan)
                {
                    $total += $tunjangan->nominal;
                    
                    echo "
                    <tr>
                        <td>Tunjangan $tunjangan->nama</td>
                        <td>+$tunjangan->nominal</td>
                    </tr>
                    ";
                }
            ?>
            <tr>
                <td>Bonus</td>
                <td>+<?php echo $bonus; $total += $bonus; ?></td>
            </tr>
            <?php
                foreach($utangs as $utang)
                {
                    $total -= $utang->nominal;
                    
                    echo "
                    <tr>
                        <td>Utang $utang->tanggal</td>
                        <td>-$utang->nominal</td>
                    </tr>
                    ";
                }
            ?>
            <tr>
                <td>Total</td>
                <td><?php echo $total; ?></td>
            </tr>
        </table>
    </body>
    <script>
        window.print()
        window.onafterprint = function(){
            window.location = "<?php echo base_url() ?>Gaji/riwayat" 
        }
    </script>
</html>
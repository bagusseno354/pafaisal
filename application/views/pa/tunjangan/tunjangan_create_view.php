<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <?= $sidebar_left ?>

        <!-- START CONTENT -->
        <section id="content">

            <?= $breadcrumbs ?>

            <!--Basic Form-->
            <div id="basic-form" class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <div class="row">
                                <form action="" method="POST" class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Nama Tunjangan</label>
                                            <input required type="text" name="nama" class="form-control"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Nominal</label>
                                            <input required type="number" name="nominal" class="form-control"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>    
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Tipe</label>
                                            <input required type="text" name="tipe" class="form-control"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Bulan</label>
                                            <input required type="number" name="bulan" class="form-control"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Tahun</label>
                                            <input required type="number" name="tahun" class="form-control"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>ID Pegawai</label>
                                            <select required id='id_minuman' name='id_pegawai'>
                                                <?php
                                                foreach ($pegawais as $k => $v) {
                                                    echo "<option value='$v->id_pegawai'>$v->id_pegawai</option>";
                                                }
                                                ?>
                                            </select>                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="btnsubmit" class="cyan waves-effect waves-light btn">Simpan<i class="mdi-content-send right"></i></button>
                                            </button>
                                            <a href="<?= base_url() ?>laporan/input_pendapatan" class="btn waves-effect waves-light red"><i class=" mdi-content-clear"></i></a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--start container-->
            <div class="container">

            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <?= $sidebar_right ?>

    </div>
    <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>

<script>
    $(document).ready(() => {

        $("#tanggal-mulai").change(() => {

            var date = new Date($("#tanggal-mulai").val())
            date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
            $("#tanggal-mulai").val(date)

            console.log($("#tanggal-mulai").val())
        })

        $("#tanggal-selesai").change(() => {

            var date = new Date($("#tanggal-selesai").val())
            date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
            $("#tanggal-selesai").val(date)

            console.log($("#tanggal-selesai").val())
        })
    })
</script>
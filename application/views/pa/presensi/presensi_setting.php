<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
	<div id="loader"></div>
	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
	<!-- START WRAPPER -->
	<div class="wrapper">

		<?= $sidebar_left ?>

		<!-- START CONTENT -->
		<section id="content">

			<?= $breadcrumbs ?>

			<!--start container-->
			<div class="container">

				<div class="divider"></div>
				<!--End Tambah Button-->

				<!--DataTables-->
				<div id="table-datatables">
					<div class="row">
						<div class="col s4 m8 l12">

							<table id="data-table-simple" class="responsive-table display excel-table" cellspacing="0" style="text-align: center">
								<thead>
									<tr>
										<th width="10%">Hari</th>
										<th width="10%">Jam tiba mulai</th>
										<th width="10%">Jam tiba selesai</th>
										<th width="10%">Jam pulang mulai</th>
										<th width="10%">Jam pulang selesai</th>
										<th width="10%">Edit</th>
									</tr>
								</thead>

								<tbody>
									<?php
									foreach ($rows as $row) {
									?>
										<tr>
											<td><?php echo $row->hari; ?></td>
											<td><?php echo $row->jam_tiba_mulai; ?></td>
											<td><?php echo $row->jam_tiba_selesai; ?></td>
											<td><?php echo $row->jam_pulang_mulai; ?></td>
											<td><?php echo $row->jam_pulang_selesai; ?></td>
											<td>
												<a href="../edit/<?php echo $row->id; ?>">Ubah</a>
											</td>
										</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End DataTables -->

			</div>
			<!--end container-->
		</section>
		<!-- END CONTENT -->

		<?= $sidebar_right ?>

	</div>
	<!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>
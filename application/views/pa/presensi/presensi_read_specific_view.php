<?= $head ?>
<?php

// print_r($rows[0]) ;

?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
	<div id="loader"></div>
	<div class="loader-section section-left"></div>
	<div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
	<!-- START WRAPPER -->
	<div class="wrapper">

		<?= $sidebar_left ?>

		<!-- START CONTENT -->
		<section id="content">

			<?= $breadcrumbs ?>

			<!--start container-->
			<div class="container">

				<div class="divider"></div>
				<!--End Tambah Button-->

				<!--DataTables-->
				<div id="table-datatables">
					<div class="row">
						<div class="col s4 m8 l12">
							<div>
								<div class="row" style="">
									<select class="col s6" id="bulan">
										<option value="1" <?php echo $month == 1 ? "selected" : "" ?>>Januari</option>
										<option value="2" <?php echo $month == 2 ? "selected" : "" ?>>Februari</option>
										<option value="3" <?php echo $month == 3 ? "selected" : "" ?>>Maret</option>
										<option value="4" <?php echo $month == 4 ? "selected" : "" ?>>April</option>
										<option value="5" <?php echo $month == 5 ? "selected" : "" ?>>Mei</option>
										<option value="6" <?php echo $month == 6 ? "selected" : "" ?>>Juni</option>
										<option value="7" <?php echo $month == 7 ? "selected" : "" ?>>Juli</option>
										<option value="8" <?php echo $month == 8 ? "selected" : "" ?>>Agustus</option>
										<option value="9" <?php echo $month == 9 ? "selected" : "" ?>>September</option>
										<option value="10" <?php echo $month == 10 ? "selected" : "" ?>>Oktober</option>
										<option value="11" <?php echo $month == 11 ? "selected" : "" ?>>November</option>
										<option value="12" <?php echo $month == 12 ? "selected" : "" ?>>Desember</option>
									</select>
									<input class="col s6" value="<?php echo isset($year) ? $year : '' ?>" type="number" id="tahun" placeholder="Tahun" />
								</div>
								<button id="tampilkan" class="cyan waves-effect waves-light btn left" style="width: 100%; height:3rem; margin: 1rem auto">
									<i class="mdi-content-add left"></i>TAMPILKAN
								</button>
</div>
							<table id="data-table-simple" class="responsive-table display excel-table" cellspacing="0" style="text-align: center">
								<thead>
									<tr>
										<th width="10%">Tanggal</th>
										<th width="10%">Pegawai</th>
									</tr>
								</thead>

								<tbody>
									<?php

									function map($row)
									{
										return ($row->tanggal);
									}

									$tanggals = array_map('map', $rows);
									$lastDate = (new DateTime("$year-$month-01"))->modify('last day of');
									$periods = new DatePeriod(
										new DateTime("$year-$month-01"),
										new DateInterval('P1D'),
										$lastDate
								   );
									   
								   foreach ($periods as $key => $value) 
								   {
								   ?>
									   <tr>
											<td style="<?php 
																							
											$tanggal = $value->format('Y-m-d');
											$time = $value->format('hh:mm:ss');

											if(in_array($tanggal, $tanggals))
											{
												if(strtotime($time) <= strtotime($haris[$value->format('N')-1]->jam_tiba_selesai))
												{
													echo("background: green"); 
												}
												else	
												{
													echo("background: yellow"); 
												}
											}
											else
												echo("background: red");													
												
											?>"><?php echo $value->format('Y-m-d'); ?></td>
											<td><?php echo $pegawai->nama_pegawai ?></td>
									   </tr>
								   <?php
								   }
									?>									
								</tbody>
							</table>
							<p>
								<b>Total masuk</b>: <?php echo count($rows) ?>
							</p>
						</div>
					</div>
				</div>
				<!-- End DataTables -->

			</div>
			<!--end container-->
		</section>
		<!-- END CONTENT -->

		<?= $sidebar_right ?>

	</div>
	<!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>

<script>
	$('#tampilkan').click(e =>
	{
		let bulan = $('#bulan').val();
		let tahun = $('#tahun').val();
		window.location = `<?php echo base_url() ?>presensi/riwayat/<?php echo $id_pegawai ?>/${bulan}/${tahun}`
	})

</script>
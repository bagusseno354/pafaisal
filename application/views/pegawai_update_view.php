<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <?= $sidebar_left ?>

        <!-- START CONTENT -->
        <section id="content">

            <?= $breadcrumbs ?>

            <!--Basic Form-->
            <div id="basic-form" class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <div class="row">
                                <form action="storeupdate" method="POST" class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>ID Pegawai</label>
                                            <input required type="text" name="id_pegawai" readonly value="<?php echo $row->id_pegawai ?>" class="form-control"><span class="text-danger"><?= form_error('id_pegawai') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Nama Pegawai</label>
                                            <input required type="text" name="nama_pegawai" class="form-control" value="<?php echo $row->nama_pegawai ?>"> <span class="text-danger"><?= form_error('nama_pegawai') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Alamat</label>
                                            <input required type="text" name="alamat" class="form-control" value="<?php echo $row->alamat ?>"> <span class="text-danger"><?= form_error('alamat') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Nomor telpon</label>
                                            <input required type="number" name="no_telp" class="form-control" value="<?php echo $row->no_telp ?>"> <span class="text-danger"><?= form_error('no_telp') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Username</label>
                                            <input required value="<?php echo $row->username ?>" type="text" name="username" class="form-control"><span class="text-danger"><?= form_error('username') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Password</label>
                                            <input required value="<?php echo $row->password ?>" type="password" name="password" class="form-control"><span class="text-danger"><?= form_error('password') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Tanggal mulai kerja</label>
                                            <input required value="<?php echo $row->tgl_mulai_kerja ?>" type="date" name="tgl_mulai_kerja" class="form-control"><span class="text-danger"><?= form_error('tgl_mulai_kerja') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Tanggal lahir</label>
                                            <input required value="<?php echo $row->tanggal_lahir ?>" type="date" name="tanggal_lahir" class="form-control"><span class="text-danger"><?= form_error('tanggal_lahir') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Pendidikan</label>
                                            <input required value="<?php echo $row->pendidikan ?>" type="text" name="pendidikan" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Tempat lahir</label>
                                            <input required value="<?php echo $row->tempat_lahir ?>" type="text" name="tempat_lahir" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Jenis kelamin</label>
                                            <select required name="jenis_kelamin">
                                                <option value="pria" <?php if($row->jenis_kelamin == 'pria') echo 'selected' ?>>Pria</option>
                                                <option value="perempuan" <?php if($row->jenis_kelamin == 'perempuan') echo 'selected' ?>>Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>ID RFID</label>
                                            <input required value="<?php echo $row->no_telp ?>" type="text" name="id_rfid" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Jabatan</label>
                                            <select required name="id_jabatan">
                                                <?php foreach($jabatans as $j) { ?>
                                                    <option value='<?php echo $j->id ?>' <?php if($row->id_jabatan == $j->id) echo 'selected' ?>><?php echo $j->nama ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="btnsubmit" class="cyan waves-effect waves-light btn">Simpan<i class="mdi-content-send right"></i></button>
                                            </button>
                                            <a href="<?= base_url() ?>pegawai" class="btn waves-effect waves-light red"><i class=" mdi-content-clear"></i></a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--start container-->
            <div class="container">

            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <?= $sidebar_right ?>

    </div>
    <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>

<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <?= $sidebar_left ?>

        <!-- START CONTENT -->
        <section id="content">

            <?= $breadcrumbs ?>

            <!--Basic Form-->
            <div id="basic-form" class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <div class="row">
                                <form action="" method="POST" class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Id Pegawai</label>
                                            <input required type="text" name="id_pegawai" value="<?php echo $id_string ?>" class="form-control"><span class="text-danger"><?= form_error('id_pegawai') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Nama Pegawai</label>
                                            <input required type="text" onkeypress="return /[a-z]/i.test(event.key)" name="nama_pegawai" class="form-control"><span class="text-danger"><?= form_error('nama_pegawai') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Alamat</label>
                                            <input required type="text" name="alamat" class="form-control"><span class="text-danger"><?= form_error('alamat') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Nomor</label>
                                            <input required type="text" name="no_telp" class="form-control"><span class="text-danger"><?= form_error('no_telp') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Username</label>
                                            <input required type="text" name="username" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Password</label>
                                            <input required type="password" name="password" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Tanggal mulai kerja</label>
                                            <input required type="date" name="tgl_mulai_kerja" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Tanggal lahir</label>
                                            <input required type="date" name="tanggal_lahir" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Pendidikan</label>
                                            <input required type="text" name="pendidikan" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Tempat lahir</label>
                                            <input required type="text" name="tempat_lahir" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Jenis kelamin</label>
                                            <select name="jenis_kelamin">
                                                <option value="pria">Pria</option>
                                                <option value="perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>ID RFID</label>
                                            <input required type="text" name="id_rfid" class="form-control"><span class="text-danger"><?= form_error('nomor') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col s12">
                                            <label>Jabatan</label>
                                            <select name="id_jabatan">
                                                <?php foreach($jabatans as $j) { ?>
                                                    <option value=<?php echo $j->id ?>><?php echo $j->nama ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="btnsubmit" class="cyan waves-effect waves-light btn">Simpan<i class="mdi-content-send right"></i></button>
                                            </button>
                                            <a href="<?= base_url() ?>pegawai" class="btn waves-effect waves-light red"><i class=" mdi-content-clear"></i></a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--start container-->
            <div class="container">

            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <?= $sidebar_right ?>

    </div>
    <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>
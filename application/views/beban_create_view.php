<?= $head ?>

<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<?= $header ?>

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <?= $sidebar_left ?>

        <!-- START CONTENT -->
        <section id="content">

            <?= $breadcrumbs ?>

            <!--Basic Form-->
            <div id="basic-form" class="section">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card-panel">
                            <div class="row">
                                <form action="storecreate" method="POST" class="col s12">
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Tanggal</label>
                                            <input class="datepicker" id="tanggal" type="date" id="tanggal" name="tanggal" class="form-control"><span class="text-danger"><?= form_error('tanggal') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <select class='form-control' name="kode_akun">
                                                <option value="513">Gaji karyawan</option>
                                                <option value="514">Biaya angkut</option>
                                            </select>
                                            <label>Jenis Beban</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <label>Nominal</label>
                                            <input type="number" name="nominal" class="form-control"><span class="text-danger"><?= form_error('id_kategori') ?></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="btnsubmit" class="cyan waves-effect waves-light btn">Simpan<i class="mdi-content-send right"></i></button>
                                            </button>
                                            <a href="<?= base_url() ?>laporan/input_pendapatan" class="btn waves-effect waves-light red"><i class=" mdi-content-clear"></i></a>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>

            <!--start container-->
            <div class="container">

            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <?= $sidebar_right ?>

    </div>
    <!-- END WRAPPER -->
</div>
<!-- END MAIN -->

<?= $footer ?>
<?= $scripts ?>

<script>
    $(document).ready(() => {

        $("#tanggal").change(() => {

            var date = new Date($("#tanggal").val())
            date = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()
            $("#tanggal").val(date)

            console.log($("#tanggal").val())
        })
    })
</script>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Currency
{

    public function format($currency)
    {
        $array = str_split($currency);
        $new_string = "";

        $k = 1;
        for ($i = count($array) - 1; $i >= 0; $i--) {

            $new_string = $array[$i] . $new_string;

            if ($k % 3 == 0 && $k != count($array)) {

                $new_string = "." . $new_string;
            }
            $k++;
        }

        return $new_string;
    }
}

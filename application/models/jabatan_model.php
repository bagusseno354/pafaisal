<?php

class Jabatan_model extends CI_model
{
	public $id;
	public $nama;
	public $gaji_pokok;
	public $labels = [];
	public $table_name = 'jabatan';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert()
	{
		$data = [
			'id' => $this->input->post('id'),
			'nama' => $this->input->post('nama'),
			'gaji_pokok' => $this->input->post('gaji_pokok'),
		];
		return $this->db->insert($this->table_name, $data);
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE " . $this->table_name . " SET 
			nama ='%s',
			gaji_pokok='%s',			
			where id='%d'",
			$this->nama,
			$this->gaji_pokok,
			$this->id
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM " . $this->table_name . " WHERE id='%s'", $this->id);
		$this->db->query($sql);
	}
	public function read()
	{
		$sql = "SELECT * FROM " . $this->table_name . " ORDER BY id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_last_row()
	{
		$query = $this->db->query('SELECT * FROM ' . $this->table_name . ' ORDER BY id DESC LIMIT 1');
		return $query;
	}
}

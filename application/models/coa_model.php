<?php

class coa_model extends CI_model
{
	public $kode_akun;
	public $nama_akun;
	public $header_kode_akun;
	public $labels = [];

	public function __construct()
	{
		parent::__construct();
		$this->labels = $this->_atributelabels();
		$this->load->database();
	}

	public function insert()
	{
		$data = array(
			'kode_akun' => $this->input->post('kode_akun'),
			'nama_akun' => $this->input->post('nama_akun'),
			'header_kode_akun' => $this->input->post('header_kode_akun'),
			'posisi_d_c' => $this->input->post('posisi_d_c'),
		);
		return $this->db->insert('coa', $data);
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE coa SET nama_akun ='%s', header_kode_akun='%s', posisi_d_c='%s' where kode_akun='%s'",
			$this->nama,
			$this->header,
			$this->posisi_d_c,
			$this->id
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM coa WHERE kode_akun='%s'", $this->id);
		$this->db->query($sql);
	}

	public function read()
	{
		$sql = "SELECT * FROM coa ORDER BY kode_akun";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function _atributelabels()
	{
		return [
			'kode_akun' => 'Kode Akun:',
			'nama_akun' => 'Nama Akun:',
			'header_kode_akun' => 'Header Akun:',
			'posisi_d_c' => 'Posisi:'
		];
	}
}

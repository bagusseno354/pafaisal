<?php

class Gaji_model extends CI_model
{
	public $id;
	public $id_pegawai;
	public $waktu_generate;
	public $bulan;
	public $tahun;
	public $labels = [];
	public $table_name = 'gaji';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert()
	{
		$data = [
			'id' => $this->input->post('id'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			'waktu_generate' => $this->input->post('waktu_generate'),
			'bulan' => $this->input->post('bulan'),
			'tahun' => $this->input->post('tahun'),			
		];
		return $this->db->insert($this->table_name, $data);
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE " . $this->table_name . " SET 
			id_pegawai ='%s',
			waktu_generate='%s',
			bulan='%s',
			tahun='%s'
			where id='%d'",
			$this->id_pegawai,
			$this->waktu_generate,
			$this->bulan,
			$this->tahun,
			$this->id
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM " . $this->table_name . " WHERE id='%s'", $this->id);
		$this->db->query($sql);
	}
	public function read()
	{
		$sql = "SELECT * FROM " . $this->table_name . " ORDER BY id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_last_row()
	{
		$query = $this->db->query('SELECT * FROM ' . $this->table_name . ' ORDER BY id DESC LIMIT 1');
		return $query;
	}
}

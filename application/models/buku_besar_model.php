<?php

class buku_besar_model extends CI_model
{
	public $no_akun;
	public $nama_akun;
	public $header_akun;
	public $labels = [];

	public function __construct()
	{
		parent::__construct();
		$this->labels = $this->_atributelabels();
		$this->load->database();
	}

	public function update_keterangan($id_transaksi, $keterangan)
	{
		$this->db->query("UPDATE jurnal_umum SET keterangan='$keterangan' WHERE id_transaksi='$id_transaksi'");
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM coa WHERE no_akun='%s'", $this->id);
		$this->db->query($sql);
	}

	public function read_saldo_akhir($kode_akun)
	{
		$sql = "SELECT SUM(nominal) as total FROM jurnal_umum WHERE posisi_d_c = 'd' AND tgl_jurnal < '$_GET[tahun]-$_GET[bulan]-01' AND jurnal_umum.kode_akun = $kode_akun ORDER BY id_transaksi";
		$saldo_akhir_debit = $this->db->query($sql)->result()[0]->total != NULL ? $this->db->query($sql)->result()[0]->total : 0;

		$sql = "SELECT SUM(nominal) as total FROM jurnal_umum WHERE posisi_d_c = 'c' AND tgl_jurnal < '$_GET[tahun]-$_GET[bulan]-01' AND jurnal_umum.kode_akun = $kode_akun ORDER BY id_transaksi";
		$saldo_akhir_kredit = $this->db->query($sql)->result()[0]->total != NULL ? $this->db->query($sql)->result()[0]->total : 0;

		$return['saldo_akhir_debit'] = $saldo_akhir_debit;
		$return['saldo_akhir_kredit'] = $saldo_akhir_kredit;

		return $return;
	}

	public function read_penjualan()
	{
		$sql = "SELECT jurnal_umum.*, coa.nama_akun FROM jurnal_umum JOIN coa ON jurnal_umum.kode_akun = coa.kode_akun WHERE MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND jurnal_umum.kode_akun = 411 ORDER BY id_transaksi";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_pembelian()
	{
		$sql = "SELECT jurnal_umum.*, coa.nama_akun FROM jurnal_umum JOIN coa ON jurnal_umum.kode_akun = coa.kode_akun WHERE MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND jurnal_umum.kode_akun = 113 ORDER BY id_transaksi";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_retur()
	{
		$sql = "SELECT jurnal_umum.*, coa.nama_akun FROM jurnal_umum JOIN coa ON jurnal_umum.kode_akun = coa.kode_akun WHERE MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND jurnal_umum.kode_akun = 512 ORDER BY id_transaksi";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read()
	{
		$sql = "SELECT jurnal_umum.*, coa.nama_akun FROM jurnal_umum JOIN coa ON jurnal_umum.kode_akun = coa.kode_akun WHERE MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND jurnal_umum.kode_akun = $_GET[kode_akun] ORDER BY id_transaksi";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_pendapatan()
	{
		$sql = "SELECT jurnal_umum.*, coa.nama_akun FROM jurnal_umum JOIN coa ON jurnal_umum.kode_akun = coa.kode_akun WHERE MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND jurnal_umum.kode_akun = 43 ORDER BY id_transaksi";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_pengeluaran()
	{
		$sql = "SELECT jurnal_umum.*, coa.nama_akun FROM jurnal_umum JOIN coa ON jurnal_umum.kode_akun = coa.kode_akun WHERE MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND jurnal_umum.kode_akun = 513 OR jurnal_umum.kode_akun = 514 ORDER BY id_transaksi";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_laporan_penjualan()
	{
		$sql = "SELECT * FROM penjualan JOIN nota_penjualan ON penjualan.id_jual = nota_penjualan.id_jual JOIN detail_jual ON nota_penjualan.no_nota = detail_jual.no_nota JOIN minuman ON minuman.id_minum = detail_jual.id_minum WHERE MONTH(nota_penjualan.tgl_jual) = $_GET[bulan] AND YEAR(nota_penjualan.tgl_jual) = $_GET[tahun]";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function read_laporan_pembelian()
	{
		$sql = "SELECT * FROM pembelian JOIN detail_pembelian ON detail_pembelian.id_pembelian = pembelian.id_pembelian JOIN bahan_baku ON bahan_baku.id_bahan_baku = detail_pembelian.id_bahan_baku WHERE MONTH(detail_pembelian.tanggal) = $_GET[bulan] AND YEAR(detail_pembelian.tanggal) = $_GET[tahun]";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function _atributelabels()
	{
		return [
			'no_akun' => 'No Akun:',
			'nama_akun' => 'Nama Akun:',
			'header_akun' => 'Header Akun:'
		];
	}
}

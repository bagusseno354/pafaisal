<?php

class Waktu_model extends CI_model
{
	public $hari;
	public $jam_pulang_mulai;
	public $jam_pulang_selesai;
	public $jam_tiba_mulai;
	public $jam_tiba_selesai;
	public $labels = [];
	public $table_name = 'waktu';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE " . $this->table_name . " SET 
			jam_pulang_mulai ='%s',
			jam_pulang_selesai='%s',
			jam_tiba_mulai ='%s',
			jam_tiba_selesai = '%s' 
			where hari='%s'",
			$this->jam_pulang_mulai,
			$this->jam_pulang_selesai,
			$this->jam_tiba_mulai,
			$this->jam_tiba_selesai,
			$this->hari
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM " . $this->table_name . " WHERE hari='%s'", $this->id);
		$this->db->query($sql);
	}
	public function read()
	{
		$sql = "SELECT * FROM " . $this->table_name . " ORDER BY hari";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_last_row()
	{
		$query = $this->db->query('SELECT * FROM ' . $this->table_name . ' ORDER BY hari DESC LIMIT 1');
		return $query;
	}
}

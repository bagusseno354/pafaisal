<?php

class Tunjangan_model extends CI_model
{
	public $id;
	public $nama_tunjangan;
	public $nominal;
	public $tipe;
	public $id_pegawai;
	public $labels = [];
	public $table_name = 'tunjangan';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert()
	{

		$last = $this->db->query("SELECT id_transaksi FROM jurnal_umum ORDER BY id_transaksi DESC LIMIT 1")->result()[0]->id_transaksi;

		$jurnal_umum_d = [
				'id_transaksi' => ++$last,
				'kode_akun' => 516,
				'posisi_d_c' => 'd',
				'nominal' => $_POST['nominal'],
				'transaksi' => 'penggajian'
		];

		$this->db->insert('jurnal_umum', $jurnal_umum_d);

		$jurnal_umum_d = [
				'id_transaksi' => ++$last,
				'kode_akun' => 213,
				'posisi_d_c' => 'c',
				'nominal' => $_POST['nominal'],
				'transaksi' => 'penggajian'
		];

		$this->db->insert('jurnal_umum', $jurnal_umum_d);
				
		$data = [
			'id' => $this->input->post('id'),
			'nama' => $this->input->post('nama'),
			'nominal' => $this->input->post('nominal'),
			'tipe' => $this->input->post('tipe'),
			'id_pegawai' => $this->input->post('id_pegawai'),
			'bulan' => $this->input->post('bulan'),
			'tahun' => $this->input->post('tahun'),
		];
		return $this->db->insert($this->table_name, $data);
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE " . $this->table_name . " SET 
			nama ='%s',
			nominal='%s',
			tipe = '%s',
			bulan = '%s',
			tahun = '%s',
			id_pegawai = '%s'  
			where id='%d'",
			$this->nama_tunjangan,
			$this->nominal,
			$this->tipe,
			$this->bulan,
			$this->tahun,
			$this->id_pegawai,
			$this->id
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM " . $this->table_name . " WHERE id='%s'", $this->id);
		$this->db->query($sql);
	}
	public function read()
	{
		$sql = "SELECT * FROM " . $this->table_name . " ORDER BY id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_last_row()
	{
		$query = $this->db->query('SELECT * FROM ' . $this->table_name . ' ORDER BY id DESC LIMIT 1');
		return $query;
	}
}

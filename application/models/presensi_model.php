<?php

class Presensi_model extends CI_model
{
	public $id;
	public $id_hari;
	public $waktu_masuk;
	public $waktu_keluar;
	public $id_pegawai;
	public $tanggal;
	public $labels = [];
	public $table_name = 'presensi';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert()
	{		
		// get current hari
		$this->db->where('id', date('w') + 1);
		$hari = $this->db->get('hari')->result()[0];

		// diff
        $presensitiba = date('H:m');
        $jamtibaselesai = $hari->jam_tiba_selesai;
        
        $diff = ((strtotime($presensitiba) - strtotime($jamtibaselesai)) / 60) / 15;

		// check user first
		$exist = false;

		if(!isset($_POST['id_pegawai']))
		{
			$this->db->where('id_rfid', $this->input->post('id_rfid'));
			$user = $this->db->get('pegawai')->result();

			if(count($user) > 0)
			{
				$exist = true;
				$id_pegawai = $user[0]->id_pegawai;
			}
		}
		else
		{
			$exist = true;
			$id_pegawai = $_POST['id_pegawai'];
		}


		if($exist)
		{
			$this->db->where('id_pegawai', $id_pegawai);
			$this->db->where('tanggal', date('Y-m-d'));
			$exist = $this->db->get($this->table_name)->result();

			if(count($exist) == 0)
			{				
				if(date('H:m') > $hari->jam_tiba_mulai && date('H:m') < $hari->jam_tiba_selesai)
				{
					$data = [
						'id' => $this->input->post('id'),
						'id_hari' => $hari->id,
						'waktu_masuk' => date('H:i'),
						'id_pegawai' => $id_pegawai,
						'tanggal' => date('Y-m-d'),
					];
		
					$this->db->insert($this->table_name, $data);
				}

				else if(date('H:m') > $hari->jam_pulang_mulai && date('H:m') < $hari->jam_pulang_selesai)
				{
					$data = [
						'id' => $this->input->post('id'),
						'id_hari' => $hari->id,
						'waktu_keluar' => date('H:i'),
						'id_pegawai' => $id_pegawai,
						'tanggal' => date('Y-m-d'),
					];
		
					$this->db->insert($this->table_name, $data);
				}
				else
				{
					return false;
				}
			}
			else
			{
				if(date('Hi') > $hari->jam_pulang_mulai && date('Hi') < $hari->jam_pulang_selesai)
				{
					$this->db->where('id', $exist[0]->id);
					$this->db->set('waktu_keluar', date('H:i'));
					$this->db->update($this->table_name);
				}
			}

			return true;
		}
		else
		{
			return false;
		}
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE " . $this->table_name . " SET 
			waktu_masuk ='%s',
			waktu_keluar ='%s',
			id_pegawai ='%s',
			tanggal = '%s' 
			where id='%d'",
			$this->waktu_masuk,
			$this->waktu_keluar,
			$this->id_pegawai,
			$this->tanggal,
			$this->id
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM " . $this->$table_name . " WHERE id='%s'", $this->id);
		$this->db->query($sql);
	}
	public function read()
	{
		$sql = "SELECT * FROM " . $this->table_name . " ORDER BY id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_last_row()
	{
		$query = $this->db->query('SELECT * FROM ' . $this->table_name . ' ORDER BY id DESC LIMIT 1');
		return $query;
	}
}

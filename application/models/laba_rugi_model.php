<?php

class laba_rugi_model extends CI_model
{
	public $no_akun;
	public $nama_akun;
	public $header_akun;
	public $labels = [];

	public function __construct()
	{
		parent::__construct();
		$this->labels = $this->_atributelabels();
		$this->load->database();
	}

	public function insert($id_transaksi, $kode_akun, $tgl_jurnal, $posisi_d_c, $nominal, $kelompok, $transaksi)
	{
		$data = array(
			'id_transaksi' => $id_transaksi,
			'kode_akun' => $kode_akun,
			'tgl_jurnal' => $tgl_jurnal,
			'posisi_d_c' => $posisi_d_c,
			'nominal' => $nominal,
			'kelompok' => $kelompok,
			'transaksi' => $transaksi
		);

		return $this->db->insert('jurnal_umum', $data);
		$this->db->query($sql);
	}

	public function update()
	{
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM coa WHERE no_akun='%s'", $this->id);
		$this->db->query($sql);
	}

	public function read()
	{
		$data['pendapatan'] = $this->db->query("SELECT SUM(total) as total FROM nota_penjualan WHERE MONTH(tgl_jual) = $_GET[bulan] AND YEAR(tgl_jual) = $_GET[tahun]")->result()[0]->total;
		$data['pembelian_bulan_ini'] = $this->db->query("SELECT SUM(total_jumlah) as total FROM detail_pembelian WHERE MONTH(tanggal) = $_GET[bulan] AND YEAR(tanggal) = $_GET[tahun]")->result()[0]->total;
		$data['penjualan_bulan_ini'] = $this->db->query("SELECT SUM(detail_jual.jumlah*bahan_baku.harga_satuan) as total FROM detail_jual JOIN nota_penjualan ON detail_jual.no_nota = nota_penjualan.no_nota JOIN minuman ON minuman.id_minum = detail_jual.id_minum JOIN bahan_baku ON minuman.id_bahan_baku = bahan_baku.id_bahan_baku WHERE MONTH(nota_penjualan.tgl_jual) = " . $_GET['bulan'] . " AND YEAR(nota_penjualan.tgl_jual) = " . $_GET['tahun'])->result()[0]->total;
		$data['pbd_bulan_ini_debit'] = $this->db->query("SELECT SUM(nominal) as total FROM jurnal_umum WHERE kode_akun = 112 AND MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND posisi_d_c = 'd'")->result()[0]->total;
		$data['pbd_bulan_ini_kredit'] = $this->db->query("SELECT SUM(nominal) as total FROM jurnal_umum WHERE kode_akun = 112 AND MONTH(tgl_jurnal) = $_GET[bulan] AND YEAR(tgl_jurnal) = $_GET[tahun] AND posisi_d_c = 'c'")->result()[0]->total;
		$data['persediaan_akhir'] = $data['pbd_bulan_ini_debit'] - $data['pbd_bulan_ini_kredit'];
		$data['pbd_sebelumnya_debit'] = $this->db->query("SELECT SUM(nominal) as total FROM jurnal_umum WHERE kode_akun = 112 AND tgl_jurnal < '$_GET[tahun]-$_GET[bulan]-01' AND posisi_d_c = 'd'")->result()[0]->total;
		$data['pbd_sebelumnya_kredit'] = $this->db->query("SELECT SUM(nominal) as total FROM jurnal_umum WHERE kode_akun = 112 AND tgl_jurnal < '$_GET[tahun]-$_GET[bulan]-01' AND posisi_d_c = 'c'")->result()[0]->total;
		$data['persediaan_awal'] = $data['pbd_sebelumnya_debit'] - $data['pbd_sebelumnya_kredit'];
		$data['pendapatan_bunga'] = $this->db->query("SELECT SUM(nominal) as total FROM jurnal_umum WHERE kode_akun = 43 AND MONTH(tgl_jurnal) = $_GET[bulan]")->result()[0]->total;
		$data['gaji_karyawan'] = $this->db->query("SELECT SUM(nominal) as total FROM jurnal_umum WHERE kode_akun = 513 AND MONTH(tgl_jurnal) = $_GET[bulan]")->result()[0]->total;
		$data['biaya_angkut'] = $this->db->query("SELECT SUM(nominal) as total FROM jurnal_umum WHERE kode_akun = 514 AND MONTH(tgl_jurnal) = $_GET[bulan]")->result()[0]->total;


		return $data;
	}

	public function _atributelabels()
	{
		return [
			'no_akun' => 'No Akun:',
			'nama_akun' => 'Nama Akun:',
			'header_akun' => 'Header Akun:'
		];
	}
}

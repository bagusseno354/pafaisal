<?php

class Izin_model extends CI_model
{
	public $id;
	public $tanggal_mulai;
	public $tanggal_selesai;
	public $keterangan;
	public $status;
	public $labels = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert()
	{
		$data = [
			'id' => $this->input->post('id'),
			'tanggal_mulai' => $this->input->post('tanggal_mulai'),
			'tanggal_selesai' => $this->input->post('tanggal_selesai'),
			'id_pegawai' => $_SESSION['id_pegawai'],
			'keterangan' => $this->input->post('keterangan'),
			'status' => 'pending'
		];
		return $this->db->insert('izin', $data);
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE izin SET 
			tanggal_mulai ='%s',
			tanggal_selesai='%s',
			keterangan = '%s' 
			where id='%d'",
			$this->tanggal_mulai,
			$this->tanggal_selesai,
			$this->keterangan,
			$this->id
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM izin WHERE id='%s'", $this->id);
		$this->db->query($sql);
	}
	public function read()
	{
		$sql = "SELECT * FROM izin ORDER BY id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_last_row()
	{
		$query = $this->db->query('SELECT * FROM izin ORDER BY id DESC LIMIT 1');
		return $query;
	}
}

<?php

class Bonus_model extends CI_model
{
	public $id;
	public $keterangan;
	public $labels = [];
	public $table_name = 'bonus';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function insert()
	{
		$data = [
			'id' => $this->input->post('id'),
			'keterangan' => $this->input->post('keterangan'),
			'nominal' => $this->input->post('nominal'),			
		];
		return $this->db->insert($this->table_name, $data);
	}

	public function update()
	{
		$sql = sprintf(
			"UPDATE " . $this->table_name . " SET 
			keterangan ='%s',
			nominal='%s'
			where id='%d'",
			$this->keterangan,
			$this->nominal,
			$this->id
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM " . $this->table_name . " WHERE id='%s'", $this->id);
		$this->db->query($sql);
	}
	public function read()
	{
		$sql = "SELECT * FROM " . $this->table_name . " ORDER BY id";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_last_row()
	{
		$query = $this->db->query('SELECT * FROM ' . $this->table_name . ' ORDER BY id DESC LIMIT 1');
		return $query;
	}
}

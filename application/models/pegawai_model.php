<?php

class pegawai_model extends CI_model
{
	public $id_pegawai;
	public $nama_pegawai;
	public $alamat;
	public $jenis_kelamin;
	public $tgl_mulai_kerja;
	public $tanggal_lahir;
	public $pendidikan;
	public $password;
	public $no_telp;
	public $username;
	public $id_jabatan;
	public $id_rfid;
	public $tempat_lahir;

	public $labels = [];

	public function __construct()
	{
		parent::__construct();
		$this->labels = $this->_atributelabels();
		$this->load->database();
	}

	public function insert()
	{
		$data = array(
			'id_pegawai' => $this->input->post('id_pegawai'),
			'nama_pegawai' => $this->input->post('nama_pegawai'),
			'alamat' => $this->input->post('alamat'),
			'no_telp' => $this->input->post('no_telp'),
			'jenis_kelamin' => $this->input->post('jenis_kelamin'),
			'tgl_mulai_kerja' => $this->input->post('tgl_mulai_kerja'),
			'tanggal_lahir' => $this->input->post('tanggal_lahir'),
			'pendidikan' => $this->input->post('pendidikan'),
			'password' => $this->input->post('password'),
			'username' => $this->input->post('username'),
			'tempat_lahir' => $this->input->post('tempat_lahir'),
			'id_jabatan' => $this->input->post('id_jabatan'),
			'id_rfid' => $this->input->post('id_rfid'),
		);
		return $this->db->insert('pegawai', $data);

		$this->db->query($sql);
	}
	public function update()
	{
		$sql = sprintf(
			"UPDATE pegawai SET 
			nama_pegawai ='%s', 
			alamat='%s', 
			no_telp='%s',  
			jenis_kelamin='%s',  
			tgl_mulai_kerja='%s',  
			tanggal_lahir='%s',  
			pendidikan='%s',  
			password='%s',  
			username='%s',  
			tempat_lahir='%s',
			id_jabatan='%s',
			id_rfid='%s' 
			where id_pegawai='%s'",
			$this->input->post('nama_pegawai'),
			$this->input->post('alamat'),
			$this->input->post('no_telp'),
			$this->input->post('jenis_kelamin'),
			$this->input->post('tgl_mulai_kerja'),
			$this->input->post('tanggal_lahir'),
			$this->input->post('pendidikan'),
			$this->input->post('password'),
			$this->input->post('username'),
			$this->input->post('tempat_lahir'),
			$this->input->post('id_jabatan'),
			$this->input->post('id_rfid'),
			$this->input->post('id_pegawai')
		);
		$this->db->query($sql);
	}

	public function delete()
	{
		$sql = sprintf("DELETE FROM pegawai WHERE id_pegawai='%s'", $this->id_pegawai);
		$this->db->query($sql);
	}

	public function read()
	{
		$sql = "SELECT * FROM pegawai ORDER BY id_pegawai";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function _atributelabels()
	{
		return [
			'id_pegawai' => 'Id Pegawai:',
			'nama_pegawai' => 'Nama Pegawai:',
			'alamat' => 'Alamat:',
			'no_telp' => 'Nomor :'

		];
	}
}

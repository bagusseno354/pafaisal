<?php

class topping_model extends CI_model
{
    public $id;
    public $nama;
    public $harga;
    public $id_kategori;
    public $labels = [];

    public function __construct()
    {
        parent::__construct();
        $this->labels = $this->_atributelabels();
        $this->load->database();
    }

    public function insert()
    {
        $data = [
            'id' => $this->input->post('id'),
            'nama' => $this->input->post('nama'),
            'harga' => str_replace(',', '', $this->input->post('harga')),
        ];
        return $this->db->insert('topping', $data);
    }

    // public function __construct(){
    // parent::__construct();
    // $this->labels=$this->_atributelabels();
    // $this->load->database();
    // }
    // public function insert(){
    // $data=[
    // 'id'=>$this->input->post('id'),
    // 'nama'=>$this->input->post('nama'),
    // 'harga'=>$this->input->post('harga'),
    // 'id_kategori'=>$this->input->post('id_kategori'),
    // ];
    // return $this->db->insert('topping',$data);
    // }
    public function update()
    {
        $sql = sprintf(
            "UPDATE topping SET nama ='%s',harga='%s' where id='%s'",
            $this->nama,
            $this->harga,
            $this->id
        );
        $this->db->query($sql);
    }
    // public function delete(){
    // $this->db->query('SET FOREIGN_KEY_CHECKS=0');
    // $sql=sprintf("DELETE FROM topping WHERE id='%s'",$this->id);
    // $this->db->query($sql);
    // $this->db->query('SET FOREIGN_KEY_CHECKS=1');

    public function delete()
    {
        $this->db->query('SET FOREIGN_KEY_CHECKS=0');
        $sql = sprintf("DELETE FROM topping WHERE id='%s'", $this->id);
        $this->db->query($sql);
        $this->db->query('SET FOREIGN_KEY_CHECKS=1');
    }

    public function read()
    {
        $sql = "SELECT * FROM topping ORDER BY id";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_last_row()
    {
        $query = $this->db->query('SELECT * FROM topping ORDER BY id DESC LIMIT 1');
        return $query;
    }

    public function _atributelabels()
    {
        return [
            'id' => 'ID Barang:',
            'nama' => 'Nama Barang:',
            'harga' => 'Harga:',
            'id_kategori' => 'ID Kategori'
        ];
    }
}

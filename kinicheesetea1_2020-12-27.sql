# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.11-MariaDB)
# Database: kinicheesetea1
# Generation Time: 2020-12-27 15:28:46 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table bahan_baku
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bahan_baku`;

CREATE TABLE `bahan_baku` (
  `id_bahan_baku` varchar(20) NOT NULL,
  `nama_bahan_baku` varchar(30) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `harga_satuan` varchar(30) NOT NULL,
  `jumlah_stok` int(100) NOT NULL,
  PRIMARY KEY (`id_bahan_baku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `bahan_baku` WRITE;
/*!40000 ALTER TABLE `bahan_baku` DISABLE KEYS */;

INSERT INTO `bahan_baku` (`id_bahan_baku`, `nama_bahan_baku`, `satuan`, `harga_satuan`, `jumlah_stok`)
VALUES
	('B001','bubuk choco melt cheese','30','2500',-2),
	('B002','bubuk green tea orginal cheese','30','2500',22),
	('B003','bubuk hoki taro cheese','30','2500',25),
	('B004','bubuk folksblue cheese','30','2500',0),
	('B005','coffee highway cheese','30','2500',0),
	('B006','green pink cheese','30','2500',0),
	('B007','red velvet cheese','30','2500',0),
	('B008','original thaitea','30','2500',0),
	('B009','cream cheese','10','2500',0),
	('B010','sweet bubble','','2500',0),
	('B011','rainbow','50','5000',0);

/*!40000 ALTER TABLE `bahan_baku` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bonus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bonus`;

CREATE TABLE `bonus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(100) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `bonus` WRITE;
/*!40000 ALTER TABLE `bonus` DISABLE KEYS */;

INSERT INTO `bonus` (`id`, `keterangan`, `nominal`)
VALUES
	(1,'Bonus Mingguan 21',25000);

/*!40000 ALTER TABLE `bonus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table coa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coa`;

CREATE TABLE `coa` (
  `kode_akun` int(11) NOT NULL,
  `nama_akun` varchar(100) NOT NULL,
  `header_kode_akun` int(11) NOT NULL,
  `posisi_d_c` varchar(1) NOT NULL,
  PRIMARY KEY (`kode_akun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `coa` WRITE;
/*!40000 ALTER TABLE `coa` DISABLE KEYS */;

INSERT INTO `coa` (`kode_akun`, `nama_akun`, `header_kode_akun`, `posisi_d_c`)
VALUES
	(0,'',0,''),
	(1,'Aktiva',0,'d'),
	(2,'Kewajiban',0,'c'),
	(3,'Modal',0,'c'),
	(4,'Pendapatan',0,'c'),
	(5,'Beban',0,'d'),
	(11,'Aktiva Lancar',1,'d'),
	(12,'Aktiva Tetap',1,'d'),
	(21,'Kewajiban Lancar',2,'c'),
	(22,'Kewajiban Jangka Panjang',2,'c'),
	(31,'Modal Pemilik',3,'c'),
	(41,'Pendapatan Operasional',4,'c'),
	(42,'Pendapatan Non Operasional',4,'c'),
	(43,'Pendapatan bunga',4,'d'),
	(51,'Beban Operasional',5,'d'),
	(52,'Beban Non Operasional',5,'d'),
	(111,'Kas',11,'d'),
	(112,'Persediaan Barang Dagang',11,'d'),
	(113,'Pembelian',11,'d'),
	(114,'Piutang Karyawan',11,'d'),
	(211,'Utang Gaji Pokok',21,'c'),
	(212,'Utang Bonus',21,'c'),
	(213,'Utang Tunjangan',0,'c'),
	(411,'Penjualan',41,'c'),
	(412,'Harga Pokok Penjualan',41,'d'),
	(511,'Beban Administrasi dan Umum',51,'d'),
	(512,'Retur',51,'c'),
	(513,'Gaji karyawan',51,'c'),
	(514,'Biaya angkut',51,'c'),
	(515,'Beban Bonus Pegawai',51,'d'),
	(516,'Beban Tunjangan',51,'d');

/*!40000 ALTER TABLE `coa` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detail_jual
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detail_jual`;

CREATE TABLE `detail_jual` (
  `id_minum` varchar(6) DEFAULT NULL,
  `no_nota` varchar(20) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `subtotal` int(11) DEFAULT NULL,
  KEY `fk_idminum` (`id_minum`),
  KEY `no_nota` (`no_nota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `detail_jual` WRITE;
/*!40000 ALTER TABLE `detail_jual` DISABLE KEYS */;

INSERT INTO `detail_jual` (`id_minum`, `no_nota`, `jumlah`, `subtotal`)
VALUES
	('M001','N001',5,50000),
	('M001','N002',5,20000),
	('M002','N002',5,30000),
	('M001','N003',5,50000),
	('M001','N004',5,50000);

/*!40000 ALTER TABLE `detail_jual` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table detail_pembelian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `detail_pembelian`;

CREATE TABLE `detail_pembelian` (
  `id_bahan_baku` varchar(30) DEFAULT NULL,
  `id_pembelian` varchar(30) DEFAULT NULL,
  `total_jumlah` varchar(30) DEFAULT NULL,
  `jumlah` int(10) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp(),
  KEY `fk_no` (`id_bahan_baku`),
  KEY `fk_pembelian` (`id_pembelian`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `detail_pembelian` WRITE;
/*!40000 ALTER TABLE `detail_pembelian` DISABLE KEYS */;

INSERT INTO `detail_pembelian` (`id_bahan_baku`, `id_pembelian`, `total_jumlah`, `jumlah`, `tanggal`)
VALUES
	('B001','X001','62500',25,'2020-04-12 11:58:21'),
	('B002','X001','62500',25,'2020-04-12 11:58:21'),
	('B003','X001','62500',25,'2020-04-12 11:58:21');

/*!40000 ALTER TABLE `detail_pembelian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gaji
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gaji`;

CREATE TABLE `gaji` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `waktu_generate` timestamp NULL DEFAULT current_timestamp(),
  `bulan` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `id_pegawai` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `gaji` WRITE;
/*!40000 ALTER TABLE `gaji` DISABLE KEYS */;

INSERT INTO `gaji` (`id`, `waktu_generate`, `bulan`, `tahun`, `total`, `id_pegawai`)
VALUES
	(1,'2020-12-27 21:05:40',4,2020,5100417,'P001'),
	(2,'2020-12-27 21:06:18',4,2020,5100417,'P001'),
	(3,'2020-12-27 22:27:45',4,2020,5000418,'P001');

/*!40000 ALTER TABLE `gaji` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table hari
# ------------------------------------------------------------

DROP TABLE IF EXISTS `hari`;

CREATE TABLE `hari` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hari` tinytext DEFAULT NULL,
  `jam_tiba_mulai` time DEFAULT NULL,
  `jam_tiba_selesai` time DEFAULT NULL,
  `jam_pulang_mulai` time DEFAULT NULL,
  `jam_pulang_selesai` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `hari` WRITE;
/*!40000 ALTER TABLE `hari` DISABLE KEYS */;

INSERT INTO `hari` (`id`, `hari`, `jam_tiba_mulai`, `jam_tiba_selesai`, `jam_pulang_mulai`, `jam_pulang_selesai`)
VALUES
	(1,'senin','01:00:00','22:00:00','22:00:00','22:15:00'),
	(2,'selasa','01:00:00','01:15:00','22:00:00','22:15:00'),
	(3,'rabu','01:00:00','01:00:00','12:00:00','14:00:00'),
	(4,'kamis','01:00:00','01:15:00','22:00:00','22:15:00'),
	(5,'jumat\n','01:00:00','01:15:00','22:00:00','22:15:00'),
	(6,'sabtu','01:00:00','01:15:00','22:00:00','22:15:00'),
	(7,'minggu','01:00:00','01:15:00','22:22:00','22:15:00');

/*!40000 ALTER TABLE `hari` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table izin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `izin`;

CREATE TABLE `izin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status` tinytext DEFAULT NULL,
  `id_pegawai` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `izin` WRITE;
/*!40000 ALTER TABLE `izin` DISABLE KEYS */;

INSERT INTO `izin` (`id`, `tanggal_mulai`, `tanggal_selesai`, `keterangan`, `status`, `id_pegawai`)
VALUES
	(1,NULL,NULL,NULL,'diizinkan',NULL),
	(2,NULL,NULL,NULL,'ditolak',NULL),
	(3,'2020-12-15','2020-12-15','test','ditolak',NULL),
	(4,'2020-12-17','2020-12-18','Bolos','diizinkan',NULL),
	(5,'2020-12-17','2020-12-17','test','diizinkan','P001');

/*!40000 ALTER TABLE `izin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jabatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jabatan`;

CREATE TABLE `jabatan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `gaji_pokok` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `jabatan` WRITE;
/*!40000 ALTER TABLE `jabatan` DISABLE KEYS */;

INSERT INTO `jabatan` (`id`, `nama`, `gaji_pokok`)
VALUES
	(1,'Kasir',5000000),
	(2,'asd',3213),
	(3,'qwe',123123);

/*!40000 ALTER TABLE `jabatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table jurnal_umum
# ------------------------------------------------------------

DROP TABLE IF EXISTS `jurnal_umum`;

CREATE TABLE `jurnal_umum` (
  `id_transaksi` int(11) NOT NULL,
  `kode_akun` int(11) NOT NULL,
  `tgl_jurnal` timestamp NOT NULL DEFAULT current_timestamp(),
  `posisi_d_c` varchar(1) NOT NULL,
  `nominal` double NOT NULL,
  `transaksi` varchar(100) NOT NULL,
  `keterangan` text NOT NULL DEFAULT '',
  PRIMARY KEY (`id_transaksi`,`kode_akun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `jurnal_umum` WRITE;
/*!40000 ALTER TABLE `jurnal_umum` DISABLE KEYS */;

INSERT INTO `jurnal_umum` (`id_transaksi`, `kode_akun`, `tgl_jurnal`, `posisi_d_c`, `nominal`, `transaksi`, `keterangan`)
VALUES
	(1,516,'2020-12-27 22:25:45','d',3124123,'piutang',''),
	(8,111,'2020-04-12 23:18:17','d',50000,'penjualan',''),
	(8,112,'2020-04-12 23:18:17','c',50000,'penjualan',''),
	(8,411,'2020-04-12 23:18:17','c',50000,'penjualan',''),
	(9,111,'2020-04-13 13:57:02','d',50000,'penjualan',''),
	(9,112,'2020-04-13 13:57:02','c',50000,'penjualan',''),
	(9,411,'2020-04-13 13:57:02','c',50000,'penjualan',''),
	(10,111,'2020-04-13 14:11:35','d',50000,'penjualan',''),
	(10,112,'2020-04-13 14:11:35','c',50000,'penjualan',''),
	(10,411,'2020-04-13 14:11:35','c',50000,'penjualan',''),
	(11,111,'2020-04-13 14:12:47','d',50000,'penjualan',''),
	(11,112,'2020-04-13 14:12:47','c',50000,'penjualan',''),
	(11,411,'2020-04-13 14:12:47','c',50000,'penjualan',''),
	(12,111,'2020-04-29 00:00:00','c',1000000,'',''),
	(12,515,'2020-04-29 00:00:00','d',1000000,'',''),
	(13,513,'2020-12-27 21:03:19','d',5000000,'penggajian',''),
	(14,211,'2020-12-27 21:03:19','c',5000000,'penggajian',''),
	(15,513,'2020-12-27 21:03:59','d',5000000,'penggajian',''),
	(16,211,'2020-12-27 21:03:59','c',5000000,'penggajian',''),
	(17,515,'2020-12-27 21:03:59','d',416.66666666667,'penggajian',''),
	(18,212,'2020-12-27 21:03:59','c',416.66666666667,'penggajian',''),
	(19,516,'2020-12-27 21:03:59','d',100000,'penggajian',''),
	(20,213,'2020-12-27 21:03:59','c',100000,'penggajian',''),
	(21,513,'2020-12-27 21:04:16','d',5000000,'penggajian',''),
	(22,211,'2020-12-27 21:04:16','c',5000000,'penggajian',''),
	(23,515,'2020-12-27 21:04:16','d',416.66666666667,'penggajian',''),
	(24,212,'2020-12-27 21:04:16','c',416.66666666667,'penggajian',''),
	(25,516,'2020-12-27 21:04:16','d',100000,'penggajian',''),
	(26,213,'2020-12-27 21:04:16','c',100000,'penggajian',''),
	(27,513,'2020-12-27 21:05:40','d',5000000,'penggajian',''),
	(28,211,'2020-12-27 21:05:40','c',5000000,'penggajian',''),
	(29,515,'2020-12-27 21:05:40','d',416.66666666667,'penggajian',''),
	(30,212,'2020-12-27 21:05:40','c',416.66666666667,'penggajian',''),
	(31,516,'2020-12-27 21:05:40','d',100000,'penggajian',''),
	(32,213,'2020-12-27 21:05:40','c',100000,'penggajian',''),
	(33,513,'2020-12-27 21:06:18','d',5000000,'penggajian',''),
	(34,211,'2020-12-27 21:06:18','c',5000000,'penggajian',''),
	(35,515,'2020-12-27 21:06:18','d',416.66666666667,'penggajian',''),
	(36,212,'2020-12-27 21:06:18','c',416.66666666667,'penggajian',''),
	(37,516,'2020-12-27 21:06:18','d',100000,'penggajian',''),
	(38,213,'2020-12-27 21:06:18','c',100000,'penggajian',''),
	(39,515,'2020-12-27 22:27:45','d',416.66666666667,'penggajian',''),
	(40,212,'2020-12-27 22:27:45','c',416.66666666667,'penggajian',''),
	(41,212,'2020-12-27 22:27:45','d',416.66666666667,'penggajian',''),
	(42,111,'2020-12-27 22:27:45','c',416.66666666667,'penggajian',''),
	(43,513,'2020-12-27 22:27:45','d',5000000,'penggajian',''),
	(44,211,'2020-12-27 22:27:45','c',5000000,'penggajian',''),
	(45,211,'2020-12-27 22:27:45','d',5000000,'penggajian',''),
	(46,111,'2020-12-27 22:27:45','c',5000000,'penggajian',''),
	(47,213,'2020-12-27 22:27:45','d',1,'penggajian',''),
	(48,111,'2020-12-27 22:27:45','c',1,'penggajian',''),
	(49,111,'2020-12-27 22:27:45','c',1,'penggajian','');

/*!40000 ALTER TABLE `jurnal_umum` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table kategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kategori` char(11) NOT NULL,
  `nama_kategori` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`)
VALUES
	('001','Minuman'),
	('002','topping'),
	('005','cemilan'),
	('008','makanan'),
	('009','makanan');

/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table minuman
# ------------------------------------------------------------

DROP TABLE IF EXISTS `minuman`;

CREATE TABLE `minuman` (
  `id_minum` varchar(6) NOT NULL,
  `nama_minum` varchar(20) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `id_kategori` char(11) DEFAULT NULL,
  `id_bahan_baku` varchar(25) NOT NULL,
  PRIMARY KEY (`id_minum`),
  KEY `fk_kategori` (`id_kategori`),
  KEY `fk_bahan_baku` (`id_bahan_baku`),
  CONSTRAINT `fk_bahan_baku` FOREIGN KEY (`id_bahan_baku`) REFERENCES `bahan_baku` (`id_bahan_baku`),
  CONSTRAINT `fk_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `minuman` WRITE;
/*!40000 ALTER TABLE `minuman` DISABLE KEYS */;

INSERT INTO `minuman` (`id_minum`, `nama_minum`, `harga`, `id_kategori`, `id_bahan_baku`)
VALUES
	('M001','choco melt cheese',10000,'001','B001'),
	('M002','green tea orginal ch',10000,'001','B002'),
	('M003','hoki taro cheese',10000,'001','B003'),
	('M004','folksblue cheese',10000,'001','B004'),
	('M005','coffee highway chees',10000,'001','B005'),
	('M006','green pink cheese',10000,'001','B006'),
	('M007','rainbow',100000,'005','B011');

/*!40000 ALTER TABLE `minuman` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table nota_penjualan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nota_penjualan`;

CREATE TABLE `nota_penjualan` (
  `no_nota` varchar(20) NOT NULL,
  `id_jual` char(10) NOT NULL,
  `tgl_jual` timestamp NULL DEFAULT current_timestamp(),
  `jumlah` int(10) NOT NULL,
  `total` varchar(30) DEFAULT NULL,
  `id_pegawai` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`no_nota`),
  KEY `fk_pegawai` (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `nota_penjualan` WRITE;
/*!40000 ALTER TABLE `nota_penjualan` DISABLE KEYS */;

INSERT INTO `nota_penjualan` (`no_nota`, `id_jual`, `tgl_jual`, `jumlah`, `total`, `id_pegawai`)
VALUES
	('N001','PX001','2020-04-12 23:18:17',5,'50000','P001'),
	('N002','PX002','2020-04-13 13:57:02',5,'50000','P001'),
	('N003','PX003','2020-04-13 14:11:35',2,'20000','P001'),
	('N004','PX004','2020-04-13 14:12:47',5,'50000','P001');

/*!40000 ALTER TABLE `nota_penjualan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `id_pegawai` varchar(20) NOT NULL DEFAULT '',
  `nama_pegawai` varchar(30) DEFAULT NULL,
  `alamat` varchar(30) DEFAULT NULL,
  `no_telp` varchar(30) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `tgl_mulai_kerja` varchar(100) DEFAULT NULL,
  `pendidikan` varchar(100) DEFAULT NULL,
  `tanggal_lahir` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `id_rfid` varchar(100) DEFAULT NULL,
  `id_jabatan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pegawai` WRITE;
/*!40000 ALTER TABLE `pegawai` DISABLE KEYS */;

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `alamat`, `no_telp`, `username`, `password`, `tgl_mulai_kerja`, `pendidikan`, `tanggal_lahir`, `tempat_lahir`, `jenis_kelamin`, `id_rfid`, `id_jabatan`)
VALUES
	('P001','nana','sukapura','81234567','nana','nana',NULL,NULL,NULL,NULL,NULL,'test',1),
	('P002','anisa','palem','89876554','ads','dsa',NULL,NULL,NULL,NULL,NULL,'asd',NULL),
	('P003','dewi','sukabirus','863645698',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P004','nadiah','sukapura','853285437',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P005','jihan','sukapura','891234965',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P006','isal','sukabirus','854965385',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P007','sd','asd','231','sad','adasd','01/20/2020',NULL,NULL,NULL,'pria',NULL,1),
	('P008','sdaaa','ads','d','d','d','','d','','d','pria','d',2);

/*!40000 ALTER TABLE `pegawai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pembelian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pembelian`;

CREATE TABLE `pembelian` (
  `id_pembelian` varchar(30) NOT NULL,
  `id_pegawai` varchar(20) DEFAULT NULL,
  `kd_vendor` varchar(20) DEFAULT NULL,
  `id_jurnal` int(10) NOT NULL,
  PRIMARY KEY (`id_pembelian`),
  KEY `kd_vendo` (`kd_vendor`),
  KEY `id_pegawai` (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pembelian` WRITE;
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;

INSERT INTO `pembelian` (`id_pembelian`, `id_pegawai`, `kd_vendor`, `id_jurnal`)
VALUES
	('X001','P001','V001',3);

/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table penjualan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `penjualan`;

CREATE TABLE `penjualan` (
  `id_jual` char(10) NOT NULL,
  `id_pegawai` char(10) NOT NULL,
  `status` varchar(25) NOT NULL,
  `id_jurnal` int(10) NOT NULL,
  PRIMARY KEY (`id_jual`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `penjualan` WRITE;
/*!40000 ALTER TABLE `penjualan` DISABLE KEYS */;

INSERT INTO `penjualan` (`id_jual`, `id_pegawai`, `status`, `id_jurnal`)
VALUES
	('PX001','0','',8),
	('PX002','0','',9),
	('PX003','1','',10),
	('PX004','P001','0',11);

/*!40000 ALTER TABLE `penjualan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table piutang
# ------------------------------------------------------------

DROP TABLE IF EXISTS `piutang`;

CREATE TABLE `piutang` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_pegawai` varchar(100) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `nominal` int(11) DEFAULT NULL,
  `tanggal` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `piutang` WRITE;
/*!40000 ALTER TABLE `piutang` DISABLE KEYS */;

INSERT INTO `piutang` (`id`, `id_pegawai`, `status`, `nominal`, `tanggal`)
VALUES
	(1,'P001','diizinkan',NULL,'2020-12-21 12:25:13'),
	(2,'P001','diizinkan',3124123,'2020-12-21 12:31:08');

/*!40000 ALTER TABLE `piutang` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table presensi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `presensi`;

CREATE TABLE `presensi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_hari` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `waktu_masuk` varchar(100) DEFAULT NULL,
  `id_pegawai` varchar(10) DEFAULT NULL,
  `waktu_keluar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `presensi` WRITE;
/*!40000 ALTER TABLE `presensi` DISABLE KEYS */;

INSERT INTO `presensi` (`id`, `id_hari`, `tanggal`, `waktu_masuk`, `id_pegawai`, `waktu_keluar`)
VALUES
	(1,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,'2020-12-15',NULL,NULL,NULL),
	(3,NULL,'2020-12-15',NULL,NULL,NULL),
	(4,NULL,'2020-12-15',NULL,NULL,NULL),
	(5,NULL,'2020-12-15',NULL,NULL,NULL),
	(6,NULL,'2020-12-16','0102',NULL,NULL),
	(7,NULL,'2020-12-16','1304',NULL,NULL),
	(8,NULL,'2020-12-16','1304',NULL,NULL),
	(9,NULL,'2020-12-16','13:05',NULL,NULL),
	(10,NULL,'2020-12-16','13:06',NULL,NULL),
	(12,3,'2020-12-16',NULL,'P001','13:28'),
	(13,1,'2020-12-21','10:55',NULL,NULL),
	(14,1,'2020-12-21','10:56',NULL,NULL),
	(15,1,'2020-12-21','10:58','P006',NULL);

/*!40000 ALTER TABLE `presensi` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table retur_pembelian
# ------------------------------------------------------------

DROP TABLE IF EXISTS `retur_pembelian`;

CREATE TABLE `retur_pembelian` (
  `id_retur` varchar(20) NOT NULL,
  `tgl_retur` date DEFAULT NULL,
  `jumlah_retur` varchar(20) DEFAULT NULL,
  `id_pembelian` varchar(30) DEFAULT NULL,
  `id_bahan_baku` varchar(20) DEFAULT NULL,
  `id_jurnal` int(10) NOT NULL,
  PRIMARY KEY (`id_retur`),
  KEY `fkid_pembelian` (`id_pembelian`),
  KEY `fkbahan_baku` (`id_bahan_baku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `retur_pembelian` WRITE;
/*!40000 ALTER TABLE `retur_pembelian` DISABLE KEYS */;

INSERT INTO `retur_pembelian` (`id_retur`, `tgl_retur`, `jumlah_retur`, `id_pembelian`, `id_bahan_baku`, `id_jurnal`)
VALUES
	('R001','2020-04-12','5','X001','B001',5);

/*!40000 ALTER TABLE `retur_pembelian` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table transaksi_coa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transaksi_coa`;

CREATE TABLE `transaksi_coa` (
  `transaksi` varchar(100) NOT NULL,
  `kode_akun` int(11) NOT NULL,
  `posisi` varchar(1) NOT NULL,
  `kelompok` int(11) NOT NULL,
  PRIMARY KEY (`transaksi`,`kode_akun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `transaksi_coa` WRITE;
/*!40000 ALTER TABLE `transaksi_coa` DISABLE KEYS */;

INSERT INTO `transaksi_coa` (`transaksi`, `kode_akun`, `posisi`, `kelompok`)
VALUES
	('pembebanan',111,'c',1),
	('pembebanan',511,'d',1),
	('pembelian',111,'c',1),
	('pembelian',112,'d',1),
	('pemodalan',31,'c',1),
	('pemodalan',111,'d',1),
	('penjualan',111,'d',1),
	('penjualan',112,'c',2),
	('penjualan',411,'c',1),
	('penjualan',412,'d',2);

/*!40000 ALTER TABLE `transaksi_coa` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tunjangan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tunjangan`;

CREATE TABLE `tunjangan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `nominal` int(20) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  `id_pegawai` varchar(11) DEFAULT NULL,
  `bulan` int(11) DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `tunjangan` WRITE;
/*!40000 ALTER TABLE `tunjangan` DISABLE KEYS */;

INSERT INTO `tunjangan` (`id`, `nama`, `nominal`, `tipe`, `id_pegawai`, `bulan`, `tahun`)
VALUES
	(1,'Transport',50000,'test',NULL,NULL,NULL),
	(2,'Makan siang',50000,'test',NULL,NULL,NULL),
	(4,'as',12,'asd','P004',NULL,NULL),
	(5,'a',1,'s','P001',1,1);

/*!40000 ALTER TABLE `tunjangan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `password`)
VALUES
	(1,'admin','admin');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table vendor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `vendor`;

CREATE TABLE `vendor` (
  `kd_vendor` varchar(20) NOT NULL,
  `nama_vendor` varchar(30) DEFAULT NULL,
  `alamat` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`kd_vendor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;

INSERT INTO `vendor` (`kd_vendor`, `nama_vendor`, `alamat`)
VALUES
	('V001','wina','palem'),
	('V002','tasya','pbb'),
	('V003','fatur','sukapura'),
	('V004','ibnuuu','sukabirus'),
	('V005','tet2','test');

/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

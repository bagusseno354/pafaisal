# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.11-MariaDB)
# Database: kinicheesetea1
# Generation Time: 2020-12-17 03:12:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table izin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `izin`;

CREATE TABLE `izin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal_mulai` date DEFAULT NULL,
  `tanggal_selesai` date DEFAULT NULL,
  `keterangan` tinytext DEFAULT NULL,
  `status` tinytext DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `izin` WRITE;
/*!40000 ALTER TABLE `izin` DISABLE KEYS */;

INSERT INTO `izin` (`id`, `tanggal_mulai`, `tanggal_selesai`, `keterangan`, `status`)
VALUES
	(1,NULL,NULL,NULL,'diizinkan'),
	(2,NULL,NULL,NULL,'ditolak'),
	(3,'2020-12-15','2020-12-15','test','ditolak'),
	(4,'2020-12-17','2020-12-18','Bolos','diizinkan');

/*!40000 ALTER TABLE `izin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pegawai
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pegawai`;

CREATE TABLE `pegawai` (
  `id_pegawai` varchar(20) NOT NULL DEFAULT '',
  `nama_pegawai` varchar(30) DEFAULT NULL,
  `alamat` varchar(30) DEFAULT NULL,
  `no_telp` varchar(30) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `tgl_mulai_kerja` varchar(100) DEFAULT NULL,
  `pendidikan` varchar(100) DEFAULT NULL,
  `tanggal_lahir` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `id_rfid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `pegawai` WRITE;
/*!40000 ALTER TABLE `pegawai` DISABLE KEYS */;

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `alamat`, `no_telp`, `username`, `password`, `tgl_mulai_kerja`, `pendidikan`, `tanggal_lahir`, `tempat_lahir`, `jenis_kelamin`, `id_rfid`)
VALUES
	('P001','nana','sukapura','81234567','nana','nana',NULL,NULL,NULL,NULL,NULL,'test'),
	('P002','anisa','palem','89876554',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P003','dewi','sukabirus','863645698',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P004','nadiah','sukapura','853285437',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P005','jihan','sukapura','891234965',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	('P006','isal','sukabirus','854965385',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `pegawai` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table presensi
# ------------------------------------------------------------

DROP TABLE IF EXISTS `presensi`;

CREATE TABLE `presensi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_hari` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `waktu_masuk` varchar(100) DEFAULT NULL,
  `id_pegawai` varchar(10) DEFAULT NULL,
  `waktu_keluar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `presensi` WRITE;
/*!40000 ALTER TABLE `presensi` DISABLE KEYS */;

INSERT INTO `presensi` (`id`, `id_hari`, `tanggal`, `waktu_masuk`, `id_pegawai`, `waktu_keluar`)
VALUES
	(1,NULL,NULL,NULL,NULL,NULL),
	(2,NULL,'2020-12-15',NULL,NULL,NULL),
	(3,NULL,'2020-12-15',NULL,NULL,NULL),
	(4,NULL,'2020-12-15',NULL,NULL,NULL),
	(5,NULL,'2020-12-15',NULL,NULL,NULL),
	(6,NULL,'2020-12-16','0102',NULL,NULL),
	(7,NULL,'2020-12-16','1304',NULL,NULL),
	(8,NULL,'2020-12-16','1304',NULL,NULL),
	(9,NULL,'2020-12-16','13:05',NULL,NULL),
	(10,NULL,'2020-12-16','13:06',NULL,NULL),
	(12,3,'2020-12-16',NULL,'P001','13:28');

/*!40000 ALTER TABLE `presensi` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

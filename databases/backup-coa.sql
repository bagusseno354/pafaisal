# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.11-MariaDB)
# Database: kinicheesetea1
# Generation Time: 2021-01-13 14:19:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table coa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `coa`;

CREATE TABLE `coa` (
  `kode_akun` int(11) NOT NULL,
  `nama_akun` varchar(100) NOT NULL,
  `header_kode_akun` int(11) NOT NULL,
  `posisi_d_c` varchar(1) NOT NULL,
  PRIMARY KEY (`kode_akun`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `coa` WRITE;
/*!40000 ALTER TABLE `coa` DISABLE KEYS */;

INSERT INTO `coa` (`kode_akun`, `nama_akun`, `header_kode_akun`, `posisi_d_c`)
VALUES
	(0,'',0,''),
	(1,'Aktiva',0,'d'),
	(2,'Kewajiban',0,'c'),
	(3,'Modal',0,'c'),
	(4,'Pendapatan',0,'c'),
	(5,'Beban',0,'d'),
	(11,'Aktiva Lancar',1,'d'),
	(12,'Aktiva Tetap',1,'d'),
	(21,'Kewajiban Lancar',2,'c'),
	(22,'Kewajiban Jangka Panjang',2,'c'),
	(31,'Modal Pemilik',3,'c'),
	(41,'Pendapatan Operasional',4,'c'),
	(42,'Pendapatan Non Operasional',4,'c'),
	(43,'Pendapatan bunga',4,'d'),
	(51,'Beban Operasional',5,'d'),
	(52,'Beban Non Operasional',5,'d'),
	(111,'Kas',11,'d'),
	(112,'Persediaan Barang Dagang',11,'d'),
	(113,'Pembelian',11,'d'),
	(114,'Piutang Karyawan',11,'d'),
	(211,'Utang Gaji Pokok',21,'c'),
	(212,'Utang Bonus',21,'c'),
	(213,'Utang Tunjangan',0,'c'),
	(411,'Penjualan',41,'c'),
	(412,'Harga Pokok Penjualan',41,'d'),
	(511,'Beban Administrasi dan Umum',51,'d'),
	(512,'Retur',51,'c'),
	(513,'Gaji karyawan',51,'c'),
	(514,'Biaya angkut',51,'c'),
	(515,'Beban Bonus Pegawai',51,'d'),
	(516,'Beban Tunjangan',51,'d');

/*!40000 ALTER TABLE `coa` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
